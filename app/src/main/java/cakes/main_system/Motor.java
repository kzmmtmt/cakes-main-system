package cakes.main_system;

import android.content.Context;

import java.util.Timer;
import java.util.TimerTask;

// モーターを動かすクラス
public class Motor{
  private int r_ratio=100,l_ratio=100; // 左右のレシオ
  private Logger logger;
  private WirelessConnectionManager connection;
  private Context c;
  private boolean isTurning=false; // 曲がる命令を実行中かどうか
  private SoundManager sm;
  private TailServo tail_servo;
  private Timer timer;

  public Motor(Logger logger,WirelessConnectionManager connection,Context c,TailServo tail_servo){
    this.logger=logger;
    this.connection=connection;
    this.c=c;
    this.tail_servo=tail_servo;
  }

  public void setSoundManager(SoundManager sm){
    this.sm=sm;
  }

  // 出力
  public void output(int r,int l){
    String command="r_motor "+r*r_ratio/100;
    logger.writeControl(c.getString(R.string.tag_motor),command);
    connection.sendToCircuit(command);
    command="l_motor "+l*l_ratio/100;
    logger.writeControl(c.getString(R.string.tag_motor),command);
    connection.sendToCircuit(command);
  }

  // 前進
  public void goAhead(int power){
    logger.writeActivity(c.getString(R.string.tag_motor),c.getString(R.string.log_goAhead)+ "("+power+")");
    tail_servo.setRun();
    output(power,power);
  }

  // 後退
  public void goBack(int power){
    logger.writeActivity(c.getString(R.string.tag_motor),c.getString(R.string.log_goBack)+ "("+power+")");
    sm.say(c.getString(R.string.sound_back));
    tail_servo.setRun();
    output(-power,-power);
  }

  // 右折
  public void turnRight(){
    if(!isTurning){
      logger.writeActivity(c.getString(R.string.tag_motor),c.getString(R.string.log_turnRight));
      sm.say(c.getString(R.string.sound_turn_right));
      turn(Parameters.motor_min_value,Parameters.motor_max_value);
    }
  }

  // 左折
  public void turnLeft(){
    if(!isTurning){
      logger.writeActivity(c.getString(R.string.tag_motor),c.getString(R.string.log_turnLeft));
      sm.say(c.getString(R.string.sound_turn_left));
      turn(Parameters.motor_max_value,Parameters.motor_min_value);
    }
  }

  public boolean isTurning(){
    return isTurning;
  }

  private void turn(int r,int l){
    final int r2=r*r_ratio/100;
    final int l2=l*l_ratio/100;
    if(!isTurning){
      isTurning=true;
      timer=new Timer(true);
      timer.scheduleAtFixedRate(new TimerTask(){
        int count=0;
        @Override
        public void run(){
          if(count==0){ // 直進
            output(r2,l2);
            tail_servo.setTurn();
          }else if(count==4){ // 終了
            goAhead(Parameters.motor_max_value);
            isTurning=false;
            tail_servo.setRun();
            cancel();
          }
          count++;
        }
      },0,Parameters.motor_turn_time);
    }
  }

  // 徐行
  public void runSlowly(){
    logger.writeActivity(c.getString(R.string.tag_motor),c.getString(R.string.log_goSlowly));
    tail_servo.setRun();
    output(Parameters.motor_speed_slowly,Parameters.motor_speed_slowly);
  }

  // 出力を0に
  public void stop(){
    logger.writeActivity(c.getString(R.string.tag_motor),c.getString(R.string.log_stopMotor));
    tail_servo.setRun();
    output(Parameters.motor_min_value,Parameters.motor_min_value);
  }

  // ブレーキ
  public void brake(){
    logger.writeActivity(c.getString(R.string.tag_motor),c.getString(R.string.log_motorBrake));
    tail_servo.setRun();
    String command="l_motor s";
    logger.writeControl(c.getString(R.string.tag_motor),command);
    connection.sendToCircuit(command);
    command="r_motor s";
    logger.writeControl(c.getString(R.string.tag_motor),command);
    connection.sendToCircuit(command);
  }

  public void onFinish(){
    try{
      isTurning=false;
      timer.cancel();
    }catch(Exception e){}
    stop();
    brake();
  }



}
