package cakes.main_system;

import android.content.Context;

// ゴールした時
public class Congratulations extends Sequence{
  public Congratulations(Context c){
    super(c);
  }

  @Override
  public void onInit(){
    if(Parameters.start_recording_in_allsequence)
      logger.stopAllLogging(false);
    logger.stopSequenceLog(false);
    cs.getSoundManager().playGoalSound();
  }

  @Override
  public void onFinish(){
    try{
      cs.getSoundManager().stopMusic();
    }catch(Exception e){}
  }

  @Override
  public void cancelSequence(){
    try{
      cs.getSoundManager().stopMusic();
    }catch(Exception e){}
  }

}
