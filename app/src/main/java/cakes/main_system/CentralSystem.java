package cakes.main_system;

import android.content.Context;

// システムを管理
public class CentralSystem{

  private SensorsManager sensor_mgr;
  private WirelessConnectionManager connection_mgr;
  private SoundManager sound_mgr;
  private Logger logger;
  private ParaServo paraservo;
  private Motor motor;
  private TailServo tail_servo;

  public CentralSystem(Context v){
    Parameters.loadSetting();
    sensor_mgr=new SensorsManager(v);
    logger=new Logger(v,sensor_mgr);
    sensor_mgr.setLogger(logger);
    sound_mgr=new SoundManager(v,logger);
    connection_mgr=new WirelessConnectionManager(v,logger);
    paraservo=new ParaServo(logger,connection_mgr,v);
    sensor_mgr.setSensors(sound_mgr);
    tail_servo=new TailServo(logger,connection_mgr,v);
    motor=new Motor(logger,connection_mgr,v,tail_servo);
    motor.setSoundManager(sound_mgr);
  }


  public SensorsManager getSensorManager(){
    return sensor_mgr;
  }

  public SoundManager getSoundManager(){
    return sound_mgr;
  }

  public Logger getLogger(){
    return logger;
  }

  public WirelessConnectionManager getWirelessConnectionManager(){
    return connection_mgr;
  }

  public ParaServo getParaServo(){
    return paraservo;
  }

  public Motor getMotor(){
    return motor;
  }

  public TailServo getTailServo(){
    return tail_servo;
  }

}
