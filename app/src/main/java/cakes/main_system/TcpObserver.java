package cakes.main_system;


/**
 * Created by Takato on 2016/05/28.
 */
public interface TcpObserver {
  void updateMessage(String message);
}