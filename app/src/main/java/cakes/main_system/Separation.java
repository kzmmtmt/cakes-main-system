package cakes.main_system;

import android.content.Context;
import android.support.v4.app.NotificationCompatSideChannelService;
import android.widget.TextView;

import java.util.Timer;
import java.util.TimerTask;

// セパレーション
public class Separation extends Sequence{

  private ParaServo paraservo;
  private Timer timer;

  public Separation(Context c){
    super(c);
    paraservo=cs.getParaServo();
    name=c.getString(R.string.sequence_separation);
  }

  @Override
  public void onInit(){
    onInit(c.getString(R.string.log_startSeparation),c.getString(R.string.sound_sequence_separation));
    if(!cs.getWirelessConnectionManager().isCircuitConnectionAlive()){
      cs.getSoundManager().say(c.getString(R.string.sound_connection_return));
      logger.writeSequence(c.getString(R.string.tag_separation),c.getString(R.string.log_waitForCircuit));
      timer=new Timer(true);
      timer.scheduleAtFixedRate(new TimerTask(){
        @Override
        public void run(){
          if(cs.getWirelessConnectionManager().isCircuitConnectionAlive()){
            cancel();
            logger.writeSequence(c.getString(R.string.tag_separation),c.getString(R.string.log_circuitConnectionEstablished));
            startSeparation();
          }
        }
      },1000,1000);
    }else
      startSeparation();
  }

  private void startSeparation(){
    paraservo.startSeparating();
    sm.say(c.getString(R.string.sound_separating));
    timer=new Timer(true);
    timer.scheduleAtFixedRate(new TimerTask(){
      @Override
      public void run(){
        if(!paraservo.isSeparating()){
          cancel();
          onFinish();
        }
      }
    },1000,1000);
  }

  @Override
  public void cancelSequence(){
    try{
      timer.cancel();
    }catch(Exception e){}
  }

}

