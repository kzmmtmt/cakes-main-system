package cakes.main_system;

import android.content.Context;

import java.util.Random;
import java.util.Timer;
import java.util.TimerTask;

// エスケーピングシーケンス
public class Escaping extends Sequence{

  private SensorsManager sensors;
  private Motor motor;
  private Timer timer;
  private TailServo tail_servo;

  public Escaping(Context c){
    super(c);
    sensors=cs.getSensorManager();
    name=c.getString(R.string.sequence_navigation);
    motor=cs.getMotor();
    tail_servo=cs.getTailServo();
  }


  @Override
  public void onInit(){
    onInit(c.getString(R.string.log_startEscaping),c.getString(R.string.sound_sequence_escaping));
    timer=new Timer(true);
    timer.scheduleAtFixedRate(new TimerTask(){
      int count=0;
      Random r=new Random();
      @Override
      public void run(){
        /*
        if(count<30){
          if(count%2==0){
            logger.writeSequence(c.getString(R.string.tag_escaping),c.getString(R.string.log_running_rut)+" ("+sensors.getLat()+" "+sensors.getLon()+")");
            tail_servo.setDegree(20);
            motor.goAhead(Parameters.motor_max_value);
          }else{
            tail_servo.setDegree(130);
            motor.goAhead(Parameters.motor_max_value/2);
          }
        }else if(count<45){ // バック
          logger.writeSequence(c.getString(R.string.tag_escaping),c.getString(R.string.log_escaping_rut)+" ("+sensors.getLat()+" "+sensors.getLon()+")");
          tail_servo.setRun();
          motor.goBack(-Parameters.motor_max_value);
        }else{ // ランダム行動
          logger.writeSequence(c.getString(R.string.tag_escaping),c.getString(R.string.log_random_escaping)+" ("+sensors.getLat()+" "+sensors.getLon()+")");
          tail_servo.setDegree(r.nextInt(171));
          motor.output(r.nextInt(1001),r.nextInt(1001));
        }
        */
        if(count==0){
          logger.writeSequence(c.getString(R.string.tag_escaping),c.getString(R.string.log_running_rut)+" ("+sensors.getLat()+" "+sensors.getLon()+")");
          motor.brake();
          tail_servo.setRun();
        }else if(count<=4)
          motor.goAhead(Parameters.motor_max_value);
        else if(count==5)
          motor.brake();
        else if(count<15)
          motor.goBack(Parameters.motor_max_value);
        else{ // ランダム行動
          logger.writeSequence(c.getString(R.string.tag_escaping),c.getString(R.string.log_random_escaping)+" ("+sensors.getLat()+" "+sensors.getLon()+")");
          tail_servo.setDegree(r.nextInt(161));
          if(r.nextInt(2)==0)
            motor.goAhead(Parameters.motor_max_value);
          else
            motor.goBack(Parameters.motor_max_value);
        }
        if(count>30)
          count=0;
        count++;
        if(!sensors.isStucking()){ // スタックを抜けたら
          logger.writeSequence(c.getString(R.string.tag_escaping),c.getString(R.string.log_escaping_finish)+" ("+sensors.getLat()+" "+sensors.getLon()+")");
          cancel();
          onFinish();
        }
      }
    },0,1000);
  }

  @Override
  public void cancelSequence(){
    motor.onFinish();
    try{
      timer.cancel();
    }catch(Exception e){}
  }


  @Override
  public void onFinish(){
    super.onFinish();
    motor.onFinish();
    try{
      timer.cancel();
    }catch(Exception e){}
  }

}
