package cakes.main_system;

import android.content.Context;
import android.preference.SwitchPreference;
import android.util.AttributeSet;
import android.view.View;

// バグを修正したswitch preference
public class BugfixSwitchPreference extends SwitchPreference{

  public BugfixSwitchPreference(Context context,AttributeSet attrs){
    super(context,attrs);
  }

  protected void onBindView(View view){
    super.onBindView(view);
  }
}
