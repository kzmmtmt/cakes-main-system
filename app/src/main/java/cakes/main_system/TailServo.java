package cakes.main_system;

import android.content.Context;

//テールサーボを制御するクラス
public class TailServo{
  private Logger logger;
  private WirelessConnectionManager connection;
  private Context c;

  public TailServo(Logger logger,WirelessConnectionManager connection,Context c){
    this.logger=logger;
    this.connection=connection;
    this.c=c;
  }

  // サーボを指定の角度にする
  public void setDegree(int degree){
    final String command=c.getString(R.string.tag_tailservo)+" "+degree;
    logger.writeControl(c.getString(R.string.tag_tailservo),command);
    connection.sendToCircuit(command);
  }

  // 走行角度に
  public void setRun(){
    logger.writeActivity(c.getString(R.string.tag_tailservo),c.getString(R.string.log_setTailservoRun));
    setDegree(Parameters.tailservo_run);
  }

  // 収納角度に
  public void setFold(){
    logger.writeActivity(c.getString(R.string.tag_tailservo),c.getString(R.string.log_setTailservoFold));
    setDegree(Parameters.tailservo_fold);
  }

  // 転回角度
  public void setTurn(){
    logger.writeActivity(c.getString(R.string.tag_tailservo),c.getString(R.string.log_setTailservoTurn));
    setDegree(Parameters.tailservo_turn);
  }

}
