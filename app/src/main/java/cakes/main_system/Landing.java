package cakes.main_system;

import android.content.Context;

import java.util.Timer;
import java.util.TimerTask;

// ランディングシーケンス
public class Landing extends Sequence{
  private SensorsManager sensors;
  private Timer timer;


  public Landing(Context c){
    super(c);
    sensors=cs.getSensorManager();
    name=c.getString(R.string.sequence_landing);
  }

  @Override
  public void onInit(){
    onInit(c.getString(R.string.log_startLanding),c.getString(R.string.sound_sequence_landing));
    cs.getSoundManager().say(c.getString(R.string.sound_landing_disconnection));
    cs.getWirelessConnectionManager().sendToCircuit("deepsleep "+Parameters.sleep_time*1000); // 回路をスリープ
    logger.writeSequence(c.getString(R.string.tag_landing),c.getString(R.string.log_disconnectToCircuit));
    cs.getTailServo().setFold();
    timer=new Timer(true);
    timer.scheduleAtFixedRate(new TimerTask(){
      int count=0;
      boolean last=false;
      double old_x=sensors.getAccel_x();
      double old_y=sensors.getAccel_y();
      double old_z=sensors.getAccel_z();
      double old_p=sensors.getPressure();
      double x,y,z,p;
      @Override
      public void run(){
        // 加速度センサーの値と気圧センサーの値が前回の値と比べてほとんど変わらなく
        x=sensors.getAccel_x();
        y=sensors.getAccel_y();
        z=sensors.getAccel_z();
        p=sensors.getPressure();
        if(Math.abs(x-old_x)<Parameters.threshold_accel_sensor&&Math.abs(y-old_y)<Parameters.threshold_accel_sensor&&Math.abs(z-old_z)<Parameters.threshold_accel_sensor&&Math.abs(p-old_p)<Parameters.threshold_pressure_sensor){
          if(last) // 連続だったならば
            count++;
          else{ // 連続でなければ
            count=1;
            sm.say(c.getString(R.string.sound_landing_judgement));
          }
          last=true;
          logger.writeSequence(c.getString(R.string.tag_landing),c.getString(R.string.log_landingCheck)+count+"/"+Parameters.seconds_landing_sensor_check+" ("+x+" "+y+" "+z+" "+p+")");
        }else{
          count=0;
          last=false;
        }
        if(count>=Parameters.seconds_landing_sensor_check){ // 一定の時間異常だったならば
          cancel();
          logger.writeSequence(c.getString(R.string.tag_landing),c.getString(R.string.log_landing));
          onFinish();
        }
        old_x=x;
        old_y=y;
        old_z=z;
        old_p=p;
      }
    },1000,1000);
  }

  @Override
  public void cancelSequence(){
    try{
      timer.cancel();
    }catch(Exception e){}
  }

}
