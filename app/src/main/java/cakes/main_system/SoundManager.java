package cakes.main_system;

import android.app.AlertDialog;
import android.content.Context;
import android.media.MediaPlayer;
import android.provider.MediaStore;
import android.speech.tts.TextToSpeech;

import java.io.IOException;
import java.util.Timer;
import java.util.TimerTask;

// 音声・音楽の管理
public class SoundManager implements TextToSpeech.OnInitListener{

  private TextToSpeech tts;
  private boolean tts_available;
  private Logger logger;
  private Context v;
  private MediaPlayer mp_goal,music,finale,se,calibration;

  public SoundManager(Context v,Logger logger){
    this.v=v;
    this.logger=logger;
    tts=new TextToSpeech(v,this);
  }

  // 音声案内
  public void say(String phrase){
    if(tts_available){
      tts.speak(phrase,TextToSpeech.QUEUE_ADD,null);
      logger.writeActivity(v.getString(R.string.tag_speak),phrase+"\n");
    }
  }

  public void say2(String phrase){
    if(tts_available){
      tts.speak(phrase,TextToSpeech.QUEUE_FLUSH,null);
      logger.writeActivity(v.getString(R.string.tag_speak),phrase+"\n");
    }
  }


  // TTSの初期化
  @Override
  public void onInit(int i){
    tts_available=(i==TextToSpeech.SUCCESS);
    if(!tts_available){
      new AlertDialog.Builder(v).setTitle(v.getString(R.string.error)).setMessage(v.getString(R.string.tts_error)).setPositiveButton(v.getString(R.string.OK),null).show();
      logger.writeActivity(v.getString(R.string.tag_error),v.getString(R.string.log_ttsDisabled));
    }
  }

  // ゴール音楽の再生
  public void playGoalSound(){
    mp_goal=MediaPlayer.create(v,R.raw.goal);
    mp_goal.setOnCompletionListener(new MediaPlayer.OnCompletionListener(){
      @Override
      public void onCompletion(MediaPlayer mediaPlayer){
        say(v.getString(R.string.sound_arriving_goal)+v.getString(R.string.sound_congratulations));
        finale=(new MediaPlayer()).create(v,R.raw.finale);
        finale.start();
      }
    });
    mp_goal.start();
  }

  public void save(int delay){
    stopBGM();
    stopMusic();
    (new Timer(true)).scheduleAtFixedRate(new TimerTask(){
      @Override
      public void run(){
        se=(new MediaPlayer()).create(v,R.raw.save);
        se.start();
        cancel();
      }
    },delay,10000);
  }


  public void stopMusic(){
    try{
      mp_goal.stop();
      finale.stop();
    }catch(Exception e){}
  }

  public void stopBGM(){
    try{
      music.stop();
    }catch(Exception e){}
  }

  public void playMusic(String title){
    stopBGM();
    if(!title.equals("")&&title!=null){
      switch(title){
        case "vader":
          music=(new MediaPlayer()).create(v,R.raw.darth);
          break;
        case "item":
          music=(new MediaPlayer()).create(v,R.raw.item);
          break;
        case "levelup":
          music=(new MediaPlayer()).create(v,R.raw.levelup);
          break;
        case "star":
          music=(new MediaPlayer()).create(v,R.raw.star);
          break;
        case "inn":
          music=(new MediaPlayer()).create(v,R.raw.yadoya);
          break;
      }
      music.start();
    }
  }

}
