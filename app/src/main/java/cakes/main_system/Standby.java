package cakes.main_system;

import android.content.Context;
import android.widget.TextView;

import java.util.Timer;
import java.util.TimerTask;

// スタンバイシーケンス
public class Standby extends Sequence{
  private SensorsManager sensors;
  private Timer timer;


  public Standby(Context c){
    super(c);
    sensors=cs.getSensorManager();
    name=c.getString(R.string.sequence_standby);
  }

  @Override
  public void onInit(){
    onInit(c.getString(R.string.log_startStandby),c.getString(R.string.sound_sequence_standby));
    cs.getTailServo().setFold();
    timer=new Timer(true);
    timer.scheduleAtFixedRate(new TimerTask(){
      int count=0;
      boolean last=false;
      @Override
      public void run(){
        // 光センサーの値が一定以上で
        if(sensors.getLight()>Parameters.threshold_light_sensor){
          if(last) // 連続だったならば
            count++;
          else{ // 連続でなければ
            count=1;
            sm.say(c.getString(R.string.sound_emitting_judgement));
          }
          last=true;
          logger.writeSequence(c.getString(R.string.tag_standby),c.getString(R.string.log_emissionCheck)+count+"/"+Parameters.seconds_light_sensor+" ("+sensors.getLight()+")");
        }else{
          count=0;
          last=false;
        }
        if(count>=Parameters.seconds_light_sensor){ // 一定の時間異常だったならば
          cancel();
          logger.writeSequence(c.getString(R.string.tag_standby),c.getString(R.string.log_emission));
          onFinish();
        }
      }
    },1000,1000);
  }


  @Override
  public void cancelSequence(){
    try{
      timer.cancel();
    }catch(Exception e){}
  }


}
