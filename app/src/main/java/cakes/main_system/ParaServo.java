package cakes.main_system;

import android.content.Context;

import java.util.Timer;
import java.util.TimerTask;

// パラサーボを制御
public class ParaServo{
  private Logger logger;
  private WirelessConnectionManager connection;
  private Context c;
  private boolean isSeparating=false;

  public ParaServo(Logger logger,WirelessConnectionManager connection,Context c){
    this.logger=logger;
    this.connection=connection;
    this.c=c;
  }


  // パラサーボをdegreeに設定
  public void setDegree(int degree){
    final String command="p_servo "+degree;
    connection.sendToCircuit(command);
    logger.writeControl(c.getString(R.string.tag_paraservo),command);
  }

  // separating
  public void startSeparating(){
    logger.writeSequence(c.getString(R.string.tag_separation),c.getString(R.string.log_startSeparating));
    isSeparating=true;
    // 1秒ごとに実行される
    (new Timer(true)).scheduleAtFixedRate(new TimerTask(){
      int count=0;
      @Override
      public void run(){
        if(count%2==0)
          setDegree(Parameters.separating_degree1);
        else
          setDegree(Parameters.separating_degree2);
        logger.writeSequence(c.getString(R.string.tag_separation),c.getString(R.string.log_separatingNow)+(count+1)+"/"+Parameters.separating_n);
        if(count>=Parameters.separating_n-1){
          cancel();
          isSeparating=false;
          logger.writeSequence(c.getString(R.string.tag_separation),c.getString(R.string.log_endSeparating));
        }
        count++;
      }
    },1000,Parameters.separating_time);
  }

  public boolean isSeparating(){
    return isSeparating;
  }

  // パラサーボを終了
  private void onFinish(){

  }


}
