package cakes.main_system;

import android.content.Context;
import android.media.MediaPlayer;

import java.util.Timer;
import java.util.TimerTask;

// オープニング
public class Opening extends Sequence{
  private MediaPlayer mp;


  public Opening(Context c){
    super(c);
  }

  @Override
  public void onInit(){
    if(!Parameters.opening)
      onFinish();
    else{
      logger.writeSequence(c.getString(R.string.tag_sequence),c.getString(R.string.log_opening));
      playOpeningMusic(9000);
    }
  }

  @Override
  public void cancelSequence(){
    try{
      mp.stop();
    }catch(Exception e){}
  }

  // n秒後にアナウンス再生
  public void playOpeningMusic(int n){
    mp=MediaPlayer.create(c,R.raw.op);
    mp.setOnCompletionListener(new MediaPlayer.OnCompletionListener(){
      @Override
      public void onCompletion(MediaPlayer mediaPlayer){
        onFinish();
      }
    });
    mp.start();
    (new Timer(true)).scheduleAtFixedRate(new TimerTask(){
      @Override
      public void run(){
        cs.getSoundManager().say(Parameters.opening_text);
        cancel();
      }
    },n,10000);
  }
}
