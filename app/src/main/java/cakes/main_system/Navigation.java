package cakes.main_system;

import android.content.Context;
import android.location.Location;

import java.util.Timer;
import java.util.TimerTask;

// ナビゲーションシーケンス
public class Navigation extends Sequence{

  private SensorsManager sensors;
  private Motor motor;
  private float distance_from_start; // スタート地点でのゴールとの距離
  private long time;
  private int announcement_count_100,announcement_count_10;
  private Timer timer;

  public Navigation(Context c){
    super(c);
    sensors=cs.getSensorManager();
    name=c.getString(R.string.sequence_navigation);
    motor=cs.getMotor();
  }


  @Override
  public void onInit(){
    onInit(c.getString(R.string.log_startNavigation),c.getString(R.string.sound_sequence_navigation));
    // GPSの取得を待ち、navigationを開始する
    cs.getTailServo().setRun();
    waitForGPS(false);
  }

  // escapingからの復帰時
  public void onRestart(){
    onInit(c.getString(R.string.log_startNavigation),c.getString(R.string.sound_sequence_navigation));
    cs.getTailServo().setRun();
    waitForGPS(true);
  }

  // GPSが高精度に受信できるまで待つ
  private void waitForGPS(final boolean from_escaping){
    if(sensors.isGetGps()&&sensors.isHighAccuracy()){ // 精度よく受信できている
      startNavigation(from_escaping);
      logger.writeSequence(c.getString(R.string.tag_navigation),c.getString(R.string.log_highAccuracyReceived));
    }else if(!sensors.isHighAccuracy()&&sensors.isGetGps()){ // 精度は良くないが受信はできている
      sm.say(c.getString(R.string.sound_trying_high_accuracy));
      logger.writeSequence(c.getString(R.string.tag_navigation),c.getString(R.string.log_tyringHighAccuracy));
      motor.runSlowly();
      (new Timer(true)).scheduleAtFixedRate(new TimerTask(){
        @Override
        public void run(){
          if(sensors.isGetGps()&&sensors.isHighAccuracy()){
            cancel();
            startNavigation(from_escaping);
          }
        }
      },1000,1000);
    }else{ // 未受信
      sm.say(c.getString(R.string.sound_receiving_gps));
      logger.writeSequence(c.getString(R.string.tag_navigation),c.getString(R.string.log_tyringToReceivingGPS));
      motor.runSlowly();
      (new Timer(true)).scheduleAtFixedRate(new TimerTask(){
        boolean first_time=true;
        @Override
        public void run(){
          if(sensors.isGetGps()&&!sensors.isHighAccuracy()){
            if(first_time){
              sm.say(c.getString(R.string.sound_trying_high_accuracy));
              logger.writeSequence(c.getString(R.string.tag_navigation),c.getString(R.string.log_tyringHighAccuracy));
              first_time=false;
            }
          }else if(sensors.isGetGps()&&sensors.isHighAccuracy()){
            cancel();
            startNavigation(from_escaping);
          }
        }
      },1000,3000);
    }
  }

  private void startNavigation(boolean from_escaping){
    motor.stop();
    if(!from_escaping){
      targetAnnouncement();
      logger.prepareKml();
      logger.writeKmlStart();
    }
    time=System.currentTimeMillis();
    final int time_to_start=from_escaping?0:10000;
    timer=new Timer(true);
    timer.scheduleAtFixedRate(new TimerTask(){
      boolean flag=true;
      @Override
      public void run(){
        if(sensors.isGetGps()&&sensors.isHighAccuracy()){ // 精度の高いGPS座標が獲得できている場合
          if(!flag){ // さっきまで精度が低かったら
            sm.say(c.getString(R.string.sound_high_accuracy_received));
            logger.writeSequence(c.getString(R.string.tag_navigation),c.getString(R.string.log_accuracyOfGpsRecovered));
          }
          // 目的地の方向を推定
          float value[]=new float[3];
          Location.distanceBetween(sensors.getLat(),sensors.getLon(),Parameters.destination[0],Parameters.destination[1],value);
          logger.getKmlGenerator().addLine(sensors.getLat(),sensors.getLon(),sensors.getAlt());
          logger.writeSequence(c.getString(R.string.tag_navigation),"("+sensors.getLat()+" "+sensors.getLon()+"): "+value[0]+"m "+value[1]+"deg");
          // ゴール地点ならば終了
          if(distanceAnnouncement(value[0])){
            logger.writeKmlGoal();
            logger.writeKml();
            cancel();
            onGoalFinish();
            return;
          }else if(sensors.isStucking()&&Parameters.escaping&&System.currentTimeMillis()-time>(Parameters.threshold_time_stuck+2)*1000+time_to_start){
            // navigationに移行してすぐだったらスタック判定しない
            cs.getSoundManager().say(c.getString(R.string.sound_stuck));
            logger.writeSequence(c.getString(R.string.tag_navigation),c.getString(R.string.log_stuckDetected));
            logger.writeKmlStuck();
            cancel();
            onFinish(); // Escapingに移行
            return;
          }
          if(!motor.isTurning()){ // 現在曲がる処理をしていない場合
            if(!turn(value[1])){ // 曲がらない場合は進む
              motor.goAhead(Parameters.motor_max_value);
              logger.writeSequence(c.getString(R.string.tag_navigation),c.getString(R.string.log_goAhead));
            }
          }
          flag=true;
        }else if(sensors.isGetGps()&&!sensors.isHighAccuracy()){ // 精度が落ちたとき
          motor.runSlowly(); // 低速走行
          if(flag){
            logger.writeSequence(c.getString(R.string.tag_navigation),c.getString(R.string.log_accuracyOfGpsDown));
            sm.say(c.getString(R.string.sound_stop_motor));
            flag=false;
          }
        }
      }
    },time_to_start,1000);
  }

  // 目的地の角度を入れると曲がるべきかを判断し、曲がる場合はtrueを返す
  private Boolean turn(double destination_direction){
    double now_direction=sensors.getAzimuth()-Parameters.rotation_angle;
    now_direction=now_direction>=0?now_direction:now_direction+360; // スマホの設置回転を考慮
    double diff=now_direction-destination_direction; // 角度の差分
    if(diff<-180)
      diff+=360;
    else if(diff>180)
      diff-=360;
    if(diff>Parameters.threshold_turn){
      motor.turnLeft();
      logger.writeKmlLeft(Math.abs(diff));
      logger.writeSequence(c.getString(R.string.tag_navigation),c.getString(R.string.log_turnLeft));
      return true;
    }else if(diff<-Parameters.threshold_turn){
      motor.turnRight();
      logger.writeKmlRight(Math.abs(diff));
      logger.writeSequence(c.getString(R.string.tag_navigation),c.getString(R.string.log_turnRight));
      return true;
    }
    return false;
  }

  // 目標地点はここから○に○m先ですと言う
  private void targetAnnouncement(){
    float value[]=new float[3];
    distance_from_start=value[0];
    Location.distanceBetween(sensors.getLat(),sensors.getLon(),Parameters.destination[0],Parameters.destination[1],value);
    String str=c.getString(R.string.sound_target)+getDirectionName(value[1])+c.getString(R.string.sound_ni)+(int)value[0]+c.getString(R.string.sound_target2)+c.getString(R.string.sound_starting_navigation);
    sm.say(str);
    logger.writeSequence(c.getString(R.string.tag_navigation),c.getString(R.string.log_startingNavigation)+Parameters.destination[0]+" "+Parameters.destination[1]+c.getString(R.string.log_startingNavigation2)+sensors.getLat()+" "+sensors.getLon()+"): "+value[0]+"m "+value[1]+"deg");
    if(value[0]>100){
      announcement_count_100=(int)value[0]/100;
      announcement_count_10=9;
    }else{
      announcement_count_100=0;
      announcement_count_10=(int)value[0]/10;
    }
  }

  // ゴールしたらtrueを返す。そうでなければ一定の間隔ごとに残りの距離を言う。
  private Boolean distanceAnnouncement(float distance){
    if(announcement_count_100>0&&distance<=announcement_count_100*100){
      sm.say(c.getString(R.string.sound_target3)+(announcement_count_100*100)+c.getString(R.string.sound_target4));
      announcement_count_100--;
    }else if(announcement_count_100==0&&announcement_count_10>0&&distance<=announcement_count_10*10){
      sm.say(c.getString(R.string.sound_target3)+(announcement_count_10*10)+c.getString(R.string.sound_target4));
      announcement_count_10--;
    }else if(distance<Parameters.threshold_goal)
      return true;
    return false;

  }

  // ゴールの方角を示す
  private String getDirectionName(double degree){
    degree=degree<0?degree+360:degree;
    if(degree>=337.5&&degree<=22.5)
      return c.getString(R.string.direction_N);
    else if(degree>22.5&&degree<67.5)
      return c.getString(R.string.direction_NE);
    else if(degree>=67.5&&degree<=112.5)
      return c.getString(R.string.direction_E);
    else if(degree>112.5&&degree<157.5)
      return c.getString(R.string.direction_SE);
    else if(degree>=157.5&&degree<=202.5)
      return c.getString(R.string.direction_S);
    else if(degree>202.5&&degree<247.5)
      return c.getString(R.string.direction_SW);
    else if(degree>=247.5&&degree<=292.5)
      return c.getString(R.string.direction_W);
    return c.getString(R.string.direction_NW);
  }

  @Override
  public void onFinish(){
    super.onFinish();
    motor.onFinish();
    try{
      timer.cancel();
    }catch(Exception e){}
  }

  @Override
  public void cancelSequence(){
    motor.onFinish();
    try{
      timer.cancel();
    }catch(Exception e){}
  }

  public void onGoalFinish(){
    motor.onFinish();
    try{
      timer.cancel();
    }catch(Exception e){}
    logger.writeSequence(c.getString(R.string.tag_navigation),c.getString(R.string.log_goal));
    super.onFinish("goal");
  }

}
