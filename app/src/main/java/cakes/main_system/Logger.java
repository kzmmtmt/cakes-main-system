package cakes.main_system;


import android.app.AlertDialog;
import android.content.Context;
import android.media.MediaPlayer;
import android.media.MediaScannerConnection;
import android.os.Environment;
import android.widget.Toast;

import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Timer;
import java.util.TimerTask;

// ログを記録
// activity_log…アクティビティー(フェーズ変化など)
// sensor_log…センサーデータ
// communication log…通信履歴
// control_log…制御履歴
// sequence_log…制御履歴
public class Logger{

  final private DateFormat df=new SimpleDateFormat("yyyy/MM/dd-HH:mm.ss"); // ログ出力用
  final private DateFormat df_file=new SimpleDateFormat("yyyyMMddHHmmss"); // ファイル名用
  private String activity_log_name=null,sensor_log_name=null,communication_log_name=null,control_log_name=null,sequence_log_name=null; // ファイル名
  private Context context;
  private SensorsManager sm;
  private PrintWriter sensor_writer=null,activity_writer=null,communication_writer=null,control_writer=null,sequence_writer=null;
  private Timer timer;
  private KmlGenerator kml;
  private boolean write_kml;

  public Logger(Context c,SensorsManager sm){
    context=c;
    this.sm=sm;
  }

  // 全ログ開始
  public boolean startAllLogging(boolean isShowing){
    if(!recordActivity(false))
      return false;
    recordCommunication(false);
    recordControl(false);
    recordSensor(false);
    if(isShowing)
      displayStartingMessage();
    return true;
  }

  // 全ログ停止
  public void stopAllLogging(boolean isShowing){
    stopActivityLog(false);
    stopCommunicationLog(false);
    stopControlLog(false);
    stopSensorLog(false);
    if(isShowing&&sensor_writer!=null&&activity_writer!=null&&communication_writer!=null&&control_writer!=null)
      displayStopMessage(activity_log_name+"\n"+communication_log_name+"\n"+control_log_name+"\n"+sensor_log_name);
  }

  public void prepareKml(){
    kml=new KmlGenerator(df.format(new Date(System.currentTimeMillis())));
    kml.addPosition("Destination","-","msn_poi",Parameters.destination[0],Parameters.destination[1],0);
    write_kml=false;
  }

  public void writeKmlLeft(double deg){
    kml.addPosition("LEFT("+String.format("%.1f",deg)+")",getTime(),"msn_L",sm.getLat(),sm.getLon(),sm.getAlt());
  }

  public void writeKmlRight(double deg){
    kml.addPosition("RIGHT("+String.format("%.1f",deg)+")",getTime(),"msn_R",sm.getLat(),sm.getLon(),sm.getAlt());
  }

  public void writeKmlStart(){
    kml.addPosition("START",getTime(),"msn_cross-hairs",sm.getLat(),sm.getLon(),sm.getAlt());
  }

  public void writeKmlGoal(){
    kml.addPosition("GOAL",getTime(),"msn_flag",sm.getLat(),sm.getLon(),sm.getAlt());
  }

  public void writeKmlStuck(){
    kml.addPosition("STUCK",getTime(),"msn_caution",sm.getLat(),sm.getLon(),sm.getAlt());
  }


  // KML出力
  public void writeKml(){
    if(!write_kml)
      try{
        String filename=Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS).getPath()+"/cakes_log/"+"report"+"_"+df_file.format(new Date(System.currentTimeMillis()))+".kml";
        File file=new File(filename);
        file.getParentFile().mkdir();
        PrintWriter pw=new PrintWriter(new OutputStreamWriter(new FileOutputStream(file,true),"UTF-8"));
        pw.write(kml.publishKml());
        pw.flush();
        pw.close();
        MediaScannerConnection.scanFile(context,new String[]{filename},null,null);
        write_kml=true;
      }catch(Exception e){
        writeFileError();
      }
  }

  // アクティビティログの記録準備
  public boolean recordActivity(boolean isShowing){
    activity_log_name=getFilename("activity");
    activity_writer=record(activity_log_name,context.getString(R.string.activity_log_header),isShowing);
    return activity_writer!=null;
  }

  // アクティビティログの書き出し
  public boolean writeActivity(String tag,String message){
    boolean success=false;
    if(activity_writer!=null)
      try{
        activity_writer.append(getTime()+","+tag+","+message+"\n");
        activity_writer.flush();
        success=true;
      }catch(Exception e){
        activity_writer=null;
        writeFileError();
      }
    return success;
  }

  // アクティビティログの保存
  public void stopActivityLog(boolean isShowing){
    stopRecording(activity_writer,activity_log_name,isShowing);
  }


  // シーケンスログの記録準備
  public boolean recordSequence(boolean isShowing){
    sequence_log_name=getFilename("sequence");
    sequence_writer=record(sequence_log_name,context.getString(R.string.sequence_log_header),isShowing);
    return sequence_writer!=null;
  }

  // アクティビティログの書き出し
  public boolean writeSequence(String sequence,String event){
    boolean success=false;
    if(sequence_writer!=null)
      try{
        sequence_writer.append(getTime()+","+sequence+","+event+"\n");
        sequence_writer.flush();
        success=true;
      }catch(Exception e){
        sequence_writer=null;
        writeFileError();
      }
    return success;
  }

  // アクティビティログの保存
  public void stopSequenceLog(boolean isShowing){
    stopRecording(sequence_writer,sequence_log_name,isShowing);
  }


  // 通信ログの記録準備
  public boolean recordCommunication(boolean isShowing){
    communication_log_name=getFilename("communication");
    communication_writer=record(communication_log_name,context.getString(R.string.communication_log_header),isShowing);
    return activity_writer!=null;
  }

  // 通信ログの書き出し
  public boolean writeCommunication(String receive_send,String from_to,String message){
    boolean success=false;
    if(communication_writer!=null)
      try{
        communication_writer.append(getTime()+","+receive_send+","+from_to+","+message+"\n");
        communication_writer.flush();
        success=true;
      }catch(Exception e){
        communication_writer=null;
        writeFileError();
      }
    return success;
  }

  // 通信ログの保存
  public void stopCommunicationLog(boolean isShowing){
    stopRecording(communication_writer,communication_log_name,isShowing);
  }

  // 制御ログの記録準備
  public boolean recordControl(boolean isShowing){
    control_log_name=getFilename("control");
    control_writer=record(control_log_name,context.getString(R.string.control_log_header),isShowing);
    return control_writer!=null;
  }

  // 制御ログの書き出し
  public boolean writeControl(String tag,String command){
    boolean success=false;
    if(control_writer!=null)
      try{
        control_writer.append(getTime()+","+tag+","+command+"\n");
        control_writer.flush();
        success=true;
      }catch(Exception e){
        control_writer=null;
        writeFileError();
      }
    return success;
  }

  // 制御ログの保存
  public void stopControlLog(boolean isShowing){
    stopRecording(control_writer,control_log_name,isShowing);
  }


  // センサーログの記録を開始
  public boolean recordSensor(boolean isShowing){
    sensor_log_name=getFilename("sensor");
    sensor_writer=record(sensor_log_name,context.getString(R.string.sensor_log_header),isShowing);
    timer=new Timer(true);
    timer.scheduleAtFixedRate(new TimerTask(){
      @Override
      public void run(){
        if(sensor_writer!=null)
          try{
            sensor_writer.append(getTime()+","+sm.getLoggingText()+","+df.format(sm.getGpsTime())+"\n");
            sensor_writer.flush();
          }catch(Exception e){
            timer.cancel();
            sensor_writer=null;
            writeFileError();
          }
      }
    },2000,1000);
    return sensor_writer!=null;
  }


  // センサーログの停止
  // 第二引数はトースト表示するかどうか
  public void stopSensorLog(boolean isShowing){
    if(sensor_writer!=null){
      timer.cancel();
      stopRecording(sensor_writer,sensor_log_name,isShowing);
    }
  }

  // return PrintWriterがnullなら失敗
  public PrintWriter record(String filename,String header,boolean isShowing){
    PrintWriter pw;
    try{
      File file=new File(filename);
      file.getParentFile().mkdir();
      pw=new PrintWriter(new OutputStreamWriter(new FileOutputStream(file,true),"UTF-8"));
      pw.append(header);
      pw.flush();
      if(isShowing)
        displayStartingMessage();
    }catch(Exception e){
      outputFileError();
      pw=null;
    }
    return pw;
  }

  // 記録の終了
  public void stopRecording(PrintWriter pw,String filename,boolean isShowing){
    if(pw!=null){
      try{
        pw.append(context.getString(R.string.eof));
        pw.flush();
        pw.close();
        MediaScannerConnection.scanFile(context,new String[]{filename},null,null);
        if(isShowing)
          displayStopMessage(filename);
      }catch(Exception e){
        writeFileError();
      }finally{
        pw=null;
      }
    }
  }


  public void setContext(Context c){
    context=c;
  }

  // コンテキストを初期設定に戻す
  public void resetContext(){
    context=Core.getContext();
  }


  // 現在時刻を出力
  private String getTime(){
    return df.format(new Date(System.currentTimeMillis()));
  }

  // ファイル名を取得
  private String getFilename(String file_name){
    return Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS).getPath()+"/cakes_log/"+file_name+"_"+df_file.format(new Date(System.currentTimeMillis()))+".csv";
  }

  // 記録開始メッセージを表示
  private void displayStartingMessage(){
    Toast.makeText(context,context.getString(R.string.start_logging),Toast.LENGTH_SHORT).show();
    final SoundManager sound=((Core)(Core.getContext())).getCentralSystem().getSoundManager();
    sound.say(context.getString(R.string.sound_start_logging));
  }

  // 記録停止メッセージを表示
  private void displayStopMessage(String filename){
    Toast.makeText(context,context.getString(R.string.stop_logging)+" "+filename,Toast.LENGTH_LONG).show();
    final SoundManager sound=((Core)(Core.getContext())).getCentralSystem().getSoundManager();
    sound.say(context.getString(R.string.sound_stop_logging));
    sound.save(1500);
  }

  // ファイルの生成エラーを表示
  private void outputFileError(){
    new AlertDialog.Builder(context).setTitle(context.getString(R.string.error)).setMessage(context.getString(R.string.generate_file_error)).setPositiveButton(context.getString(R.string.OK),null).show();
  }

  // ファイルの書き込みエラー表示
  private void writeFileError(){
    new AlertDialog.Builder(context).setTitle(context.getString(R.string.error)).setMessage(context.getString(R.string.write_file_error)).setPositiveButton(context.getString(R.string.OK),null).show();
  }

  public KmlGenerator getKmlGenerator(){
    return kml;
  }
}
