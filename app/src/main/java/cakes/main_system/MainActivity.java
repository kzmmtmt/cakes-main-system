package cakes.main_system;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Handler;
import android.speech.tts.TextToSpeech;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.util.Observable;
import java.util.Observer;
import java.util.Timer;
import java.util.TimerTask;

// 起動時に表示されるアクティビティー
public class MainActivity extends AppCompatActivity{

  private Button bt,b3;
  private Logger logger;
  private boolean logging=false;
  private TextView tv;
  private Handler handler;
  private Context act;

  @Override
  protected void onCreate(Bundle savedInstanceState){
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_main);
    Core core=(Core)getApplication();
    core.Initialize(this,getApplication());
    logger=core.getCentralSystem().getLogger();
    act=this;
    ((TextView)findViewById(R.id.tv)).setText("version: "+Parameters.version+"  /  build: "+Parameters.build);
    bt=(Button)findViewById(R.id.bt);
    bt.setOnClickListener(new View.OnClickListener() {
      public void onClick(View v) {
        startSensorMonitorActivity();
      }
    });
    ((Button)findViewById(R.id.b2)).setOnClickListener(new View.OnClickListener(){
      @Override
      public void onClick(View view){
        startRemoteControlActivity();
      }
    });

    b3=(Button)findViewById(R.id.b3);
    b3.setOnClickListener(new View.OnClickListener(){
      @Override
      public void onClick(View view){
        startAllSequence();
      }
    });

    ((Button)findViewById(R.id.b4)).setOnClickListener(new View.OnClickListener(){
      @Override
      public void onClick(View view){
        startSettingActivity();
      }
    });

    tv=(TextView)findViewById(R.id.tv2);
    handler=new Handler();

  }

  private void startSensorMonitorActivity(){
    Intent intent=new Intent(this,cakes.main_system.SensorMonitorActivity.class);
    startActivity(intent);
  }

  private void startRemoteControlActivity(){
    Intent intent=new Intent(this,cakes.main_system.RemoteControlActivity.class);
    startActivity(intent);
  }

  private void startSettingActivity(){
    Intent intent=new Intent(this,cakes.main_system.SettingActivity.class);
    startActivity(intent);
  }

  private void startAllSequence(){
    ((Core)getApplication()).getCentralSystem().getWirelessConnectionManager().connectToCircuit();
    //テキスト入力を受け付けるビューを作成します。
    final EditText editView = new EditText(MainActivity.this);
    new AlertDialog.Builder(MainActivity.this)
        .setIcon(android.R.drawable.ic_dialog_info)
        .setTitle("猶予時間(秒)を入力してください")
        .setView(editView)
        .setPositiveButton("OK", new DialogInterface.OnClickListener() {
          public void onClick(DialogInterface dialog, int whichButton) {
            try{
              final int time=Integer.valueOf(editView.getText().toString());
              if(time<0)
                Toast.makeText(MainActivity.this,"0秒以上の整数を指定してください",Toast.LENGTH_SHORT).show();
              else if(time==0){
                (new AllSequence(act,true)).startAllSequence();
                //(new AllSequence(act,true)).startSequence(3);
                b3.setEnabled(false);
                tv.setText(getString(R.string.sequence_standby));
              }else{
                b3.setEnabled(false);
                tv.setText("猶予時間: "+time+"秒");
                ((Core)getApplication()).getCentralSystem().getSoundManager().say(time+"秒後にオールシーケンス試験を開始します。");
                (new Timer(true)).scheduleAtFixedRate(new TimerTask(){
                  int count=time;

                  @Override
                  public void run(){
                    if(count>=0){
                      if(count>=30){
                        if(count%5==0)
                          ((Core)getApplication()).getCentralSystem().getSoundManager().say(count+"");
                      }else
                        ((Core)getApplication()).getCentralSystem().getSoundManager().say(count+"");
                      final String announce="猶予時間残り: "+count+"秒";
                      handler.post(new Runnable(){
                        @Override
                        public void run(){
                          tv.setText(announce);
                        }
                      });
                    }else{
                      (new AllSequence(act,true)).startAllSequence();
                      //(new AllSequence(act,true)).startSequence(3);
                      handler.post(new Runnable(){
                        @Override
                        public void run(){
                          tv.setText(getString(R.string.sequence_standby));
                        }
                      });

                      cancel();
                    }
                    count--;
                  }
                },5000,1000);
              }
            }catch(Exception e){
                Toast.makeText(MainActivity.this,"数値を指定してください",Toast.LENGTH_SHORT).show();
            }

          }
        })
        .setNegativeButton("キャンセル", new DialogInterface.OnClickListener() {
          public void onClick(DialogInterface dialog, int whichButton) {
          }
        })
        .show();
  }

  public void changeTextView(String str){
    final String s=str;
    handler.post(new Runnable(){
      @Override
      public void run(){
        tv.setText(s);
      }
    });
  }

  public void setReset(){
    handler.post(new Runnable(){
      @Override
      public void run(){
        tv.setText("Finished");
        b3.setEnabled(true);
      }
    });

  }



  @Override
  public boolean onCreateOptionsMenu(Menu menu){
    getMenuInflater().inflate(R.menu.menu_main_activity,menu);
    return true;
  }

  @Override
  public boolean onOptionsItemSelected(MenuItem item){
    // 閉じるボタン
    if(item.getItemId()==R.id.menu_close_main_activity){
      ((Core)getApplication()).getCentralSystem().getSensorManager().finish();
      ((Core)getApplication()).getCentralSystem().getWirelessConnectionManager().finish();
      logger.stopAllLogging(false);
      finish();
      return true;
      // 記録開始・停止
    }else if(item.getItemId()==R.id.menu_record){
      if(logging){
        logger.stopAllLogging(true);
        item.setIcon(android.R.drawable.ic_notification_overlay);
        item.setTitle(getString(R.string.menu_start_logging));
      }else{
        logging=!logger.startAllLogging(true);
        if(!logging){
          item.setIcon(android.R.drawable.ic_menu_save);
          item.setTitle(getString(R.string.menu_stop_logging));
        }
      }
      logging=!logging;
    }
    return super.onOptionsItemSelected(item);
  }

  @Override
  // 戻るキーを無効化
  public boolean dispatchKeyEvent(KeyEvent event) {
    if (event.getAction()==KeyEvent.ACTION_DOWN) {
      switch (event.getKeyCode()) {
        case KeyEvent.KEYCODE_BACK:
          Toast.makeText(this,getString(R.string.dont_back),Toast.LENGTH_SHORT).show();
          return true;
      }
    }
    return super.dispatchKeyEvent(event);
  }

}
