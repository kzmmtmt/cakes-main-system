package cakes.main_system;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Observable;
import java.util.Observer;
import java.util.StringTokenizer;


// 通信用アクティビティ
public class RemoteControlActivity extends AppCompatActivity implements Observer{

  private WirelessConnectionManager connection_mgr;
  private Logger logger;
  private ArrayList<String> sent_list,received_list; // 履歴
  private ScrollView sent_sv,received_sv;
  private TextView send,receive;
  private EditText command_et;
  private Activity act;
  private Handler handler;
  private AllSequence allsequence=null;
  private Motor motor;

  @Override
  protected void onCreate(Bundle savedInstanceState){
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_remote_control);
    setTitle(R.string.remote_control);
    act=this;
    // アイコン設定
    getSupportActionBar().setDisplayShowHomeEnabled(true);
    getSupportActionBar().setIcon(android.R.drawable.ic_menu_myplaces);
    Core core=(Core)getApplication();
    connection_mgr=core.getCentralSystem().getWirelessConnectionManager();
    logger=core.getCentralSystem().getLogger();
    connection_mgr.connectToController();
    connection_mgr.addObserver(this);
    sent_list=new ArrayList<String>();
    received_list=new ArrayList<String>();
    send=(TextView)findViewById(R.id.sent_log);
    receive=(TextView)findViewById(R.id.received_log);
    sent_sv=(ScrollView)findViewById(R.id.sent_scrollview);
    received_sv=(ScrollView)findViewById(R.id.receive_scrollview);
    command_et=(EditText)findViewById(R.id.command);
    motor=core.getCentralSystem().getMotor();
    logger.writeActivity(getString(R.string.tag_activity),getString(R.string.log_remoteControlStarted));
    handler=new Handler();
    ((Button)findViewById(R.id.command_button)).setOnClickListener(new View.OnClickListener(){
      @Override
      public void onClick(View view){
        String str=command_et.getText().toString();
        if(!str.equals("")){
          connection_mgr.sendToCircuit(str);
          addSentText(str);
          Toast.makeText(act,str,Toast.LENGTH_SHORT).show();
          command_et.setText("");
        }
      }
    });
    ((Button)findViewById(R.id.loopback_button)).setOnClickListener(new View.OnClickListener(){
      @Override
      public void onClick(View view){
        String str=command_et.getText().toString();
        if(!str.equals("")){
          execute(str);
          Toast.makeText(act,str,Toast.LENGTH_SHORT).show();
          command_et.setText("");
        }
      }
    });
  }

  // 送信履歴に追加
  private void addSentText(String str){
    sent_list.add(str);
    if(sent_list.size()>100)
      sent_list.remove(0);
    String buf="";
    for(int i=0;i<sent_list.size();i++)
      buf+=sent_list.get(i)+"\n";
    send.setText(buf);
    sent_sv.fullScroll(ScrollView.FOCUS_DOWN);
  }

  // 受信履歴に追加
  private void addReceivedText(String str){
    received_list.add(str);
    if(received_list.size()>100)
      received_list.remove(0);
    String buf="";
    for(int i=0;i<received_list.size();i++)
      buf+=received_list.get(i)+"\n";
    receive.setText(buf);
    received_sv.fullScroll(ScrollView.FOCUS_DOWN);
  }



  @Override
  public boolean onCreateOptionsMenu(Menu menu){
    getMenuInflater().inflate(R.menu.remote_control,menu);
    return true;
  }

  @Override
  public boolean onOptionsItemSelected(MenuItem item){
    // 閉じるボタン
    if(item.getItemId()==R.id.menu_close_remote_control){
      logger.writeActivity(getString(R.string.tag_activity),getString(R.string.log_remoteControlFinished));
      connection_mgr.disconnectToCircuit();
      connection_mgr.disconnectToController();
      finish();
      return true;
    }else if(item.getItemId()==R.id.menu_sensor_remote_control){
      Intent intent=new Intent(this,cakes.main_system.SensorMonitorActivity.class);
      startActivity(intent);
      return true;
    }else if(item.getItemId()==R.id.menu_refresh_remote_control){
      logger.writeActivity(getString(R.string.tag_connection),getString(R.string.log_remoteControlReconnect));
      connection_mgr.disconnectToCircuit();
      connection_mgr.disconnectToController();
      Toast.makeText(this,getString(R.string.reconnect),Toast.LENGTH_SHORT).show();
      ((Core)getApplication()).getCentralSystem().getSoundManager().say(getString(R.string.reconnect));
      connection_mgr.connectToCircuit();
      connection_mgr.connectToController();
      return true;
    }
    return super.onOptionsItemSelected(item);
  }

  @Override
  // 戻るキーを無効化
  public boolean dispatchKeyEvent(KeyEvent event) {
    if (event.getAction()==KeyEvent.ACTION_DOWN) {
      switch (event.getKeyCode()) {
        case KeyEvent.KEYCODE_BACK:
          Toast.makeText(this,getString(R.string.dont_back),Toast.LENGTH_SHORT).show();
          return true;
      }
    }
    return super.dispatchKeyEvent(event);
  }


  // 受信すると呼ばれる
  @Override
  public void update(Observable observable,Object o){
    final String tmp=(String)o;
    execute(tmp);
  }

  private void execute(final String tmp){
    handler.post(new Runnable(){
      @Override
      public void run(){
        addReceivedText(tmp);
        if(tmp.equals("start allsequence")){
          if(allsequence==null){
            allsequence=new AllSequence(act,false);
            allsequence.startAllSequence();
          }
        }else if(tmp.equals("start navigation")){
          if(allsequence==null){
            allsequence=new AllSequence(act,false);
            allsequence.startSequence(5);
          }
        }else if(tmp.equals("start finish")){
          if(allsequence==null){
            allsequence=new AllSequence(act,false);
            allsequence.startSequence(7);
          }
        }else if(tmp.equals("stop allsequence"))
          allsequence.onFinish();
        else if(tmp.equals("explain"))
          ((Core)getApplication()).getCentralSystem().getSoundManager().say2(Parameters.explain_text);
        else if(tmp.equals("turn left"))
          motor.turnLeft();
        else if(tmp.equals("turn right"))
          motor.turnRight();
        else if(tmp.equals("stop music"))
          ((Core)getApplication()).getCentralSystem().getSoundManager().stopBGM();
        else if(tmp.split(" ")[0].equals("play"))
          ((Core)getApplication()).getCentralSystem().getSoundManager().playMusic(tmp.split(" ")[1]);
        else{
          try{
            int a=Integer.valueOf(tmp);
            String phrase="";
            switch(a){
              case 1:
                phrase=getString(R.string.p1);
                break;
              case 2:
                phrase=getString(R.string.p2);
                break;
              case 3:
                phrase=getString(R.string.p3);
                break;
              case 4:
                phrase=getString(R.string.p4);
                break;
              case 5:
                phrase=getString(R.string.p5);
                break;
              case 6:
                phrase=getString(R.string.p6);
                break;
              case 7:
                phrase=getString(R.string.p7);
                break;
              case 8:
                phrase=getString(R.string.p8);
                break;
              case 9:
                phrase=getString(R.string.p9);
                break;
            }
            ((Core)getApplication()).getCentralSystem().getSoundManager().say2(phrase);
          }catch(Exception e){
            final String command=connection_mgr.convertCommand(tmp);
            if(!command.equals("")){
              connection_mgr.sendToCircuit(command);
              addSentText(command);
            }
          }
        }
      }
    });
  }

}
