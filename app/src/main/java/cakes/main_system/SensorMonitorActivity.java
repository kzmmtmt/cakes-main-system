package cakes.main_system;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Html;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;
import android.os.Handler;
import android.widget.Toast;

import java.util.Timer;
import java.util.TimerTask;

// センサーモニター
public class SensorMonitorActivity extends AppCompatActivity{

  private TextView sensor_view;
  private SensorsManager sm;
  private Handler handler;
  private Timer timer;
  private Logger logger;

  @Override
  protected void onCreate(Bundle savedInstanceState){
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_sensor_monitor);
    setTitle(R.string.sensor_monitor);
    // アイコン設定
    getSupportActionBar().setDisplayShowHomeEnabled(true);
    getSupportActionBar().setIcon(android.R.drawable.ic_dialog_info);
    Core core=(Core)getApplication();
    sm=core.getCentralSystem().getSensorManager();
    logger=core.getCentralSystem().getLogger();
    logger.setContext(this);
    sensor_view=(TextView)findViewById(R.id.sensor_view);
    sensor_view.setText(Html.fromHtml("<h1>"+getString(R.string.loading_sensor)+"</h1>"));
    handler=new Handler();
    updateDisplay();
    logger.writeActivity(getString(R.string.tag_activity),getString(R.string.log_sensorMonitorStarted));
  }

  private void updateDisplay(){
    // 2秒後から1秒間隔で記録
    timer=new Timer(false);
    timer.scheduleAtFixedRate(new TimerTask(){
      @Override
      public void run(){
        handler.post(new Runnable(){
          @Override
          public void run(){
            sensor_view.setText(Html.fromHtml(sm.getScreenText()));
          }
        });
      }
    },2000,1000);
  }

  // このアクティビティの終了
  public void exitSensorMonitor(){
    timer.cancel();
    logger.writeActivity(getString(R.string.tag_activity),getString(R.string.log_sensorMonitorFinished));
    logger.resetContext();
    finish();
  }


  @Override
  public boolean onCreateOptionsMenu(Menu menu){
    getMenuInflater().inflate(R.menu.sensor_monitor,menu);
    return true;
  }

  @Override
  public boolean onOptionsItemSelected(MenuItem item){
    // 閉じるボタン
    if(item.getItemId()==R.id.menu_close_sensor_monitor){
      exitSensorMonitor();
      return true;
    }
    return super.onOptionsItemSelected(item);
  }

  @Override
  // 戻るキーを無効化
  public boolean dispatchKeyEvent(KeyEvent event) {
    if (event.getAction()==KeyEvent.ACTION_DOWN) {
      switch (event.getKeyCode()) {
        case KeyEvent.KEYCODE_BACK:
          Toast.makeText(this,getString(R.string.dont_back),Toast.LENGTH_SHORT).show();
          return true;
      }
    }
    return super.dispatchKeyEvent(event);
  }

}
