package cakes.main_system;

import android.content.Context;
import android.media.MediaPlayer;

import java.util.Timer;
import java.util.TimerTask;

// キャリブレーション
public class Calibration extends Sequence{
  private MediaPlayer mp;


  public Calibration(Context c){
    super(c);
  }

  @Override
  public void onInit(){
    sm.say(c.getString(R.string.sound_allsequence));
    if(Parameters.calibration){
      logger.writeSequence(c.getString(R.string.tag_sequence),c.getString(R.string.log_calibration));
      sm.say(c.getString(R.string.sound_calibration_start));
      playCalibrationMusic(9000);
    }else
      onFinish();
  }

  @Override
  public void cancelSequence(){
    try{
      mp.stop();
    }catch(Exception e){}
  }

  // n秒後に音楽を再生
  public void playCalibrationMusic(int n){
    (new Timer(true)).scheduleAtFixedRate(new TimerTask(){
      @Override
      public void run(){
        switch(Parameters.calibration_song){
          case 0:
            mp=MediaPlayer.create(c,R.raw.calibration0);
            break;
          case 1:
            mp=MediaPlayer.create(c,R.raw.calibration1);
            break;
          case 2:
            mp=MediaPlayer.create(c,R.raw.calibration2);
            break;
          case 3:
            mp=MediaPlayer.create(c,R.raw.calibration3);
            break;
          case 4:
            mp=MediaPlayer.create(c,R.raw.calibration4);
            break;
          case 5:
            mp=MediaPlayer.create(c,R.raw.calibration5);
            break;
          case 6:
            mp=MediaPlayer.create(c,R.raw.calibration6);
            break;
          case 7:
            mp=MediaPlayer.create(c,R.raw.calibration7);
            break;
          default:
            mp=MediaPlayer.create(c,R.raw.calibration0);
        }
        mp.setOnCompletionListener(new MediaPlayer.OnCompletionListener(){
          @Override
          public void onCompletion(MediaPlayer mediaPlayer){
            cs.getSoundManager().say(c.getString(R.string.sound_calibration_finish));
            onFinish();
          }
        });
        mp.start();
        cancel();
      }
    },n,10000);
  }
}
