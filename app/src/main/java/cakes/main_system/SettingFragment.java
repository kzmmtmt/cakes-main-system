package cakes.main_system;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.preference.EditTextPreference;
import android.preference.ListPreference;
import android.preference.Preference;
import android.preference.PreferenceFragment;
import android.preference.PreferenceManager;
import android.widget.Toast;


// 設定用フラグメント
public class SettingFragment extends PreferenceFragment implements SharedPreferences.OnSharedPreferenceChangeListener{

  private EditTextPreference latitude,longitude,goal_judgement,stuck_time,stuck_num,stuck_threshold,fold_degree,run_degree,turn_degree,slow_speed,gps_timeout,gps_high_accuracy,rotation_degree,circuit_port,monitor_port,threshold_light,seconds_light,sleep_time,threshold_accel,threshold_pressure,seconds_landing,separation_n,separation_time,navigation_turn;
  private ListPreference calibration_song;
  private Context c;
  private String[] list;

  @Override
  public void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    c=getActivity();
    addPreferencesFromResource(R.xml.preferences);
    CheckNoInputListener cnil=new CheckNoInputListener();
    list=getResources().getStringArray(R.array.calibration_entries);

    latitude=(EditTextPreference)findPreference(getString(R.string.p_latitude));
    latitude.setOnPreferenceChangeListener(cnil);
    longitude=(EditTextPreference)findPreference(getString(R.string.p_longitude));
    longitude.setOnPreferenceChangeListener(cnil);

    // 現在地を目標地点にする
    findPreference(getString(R.string.p_setNowLocation)).setOnPreferenceClickListener(new Preference.OnPreferenceClickListener(){
      @Override
      public boolean onPreferenceClick(Preference preference){
        SensorsManager sm=((Core)Core.getContext()).getCentralSystem().getSensorManager();
        if(sm.isGetGps()&&sm.isHighAccuracy()){ // GPSが取得できている場合のみ
          final double lat=sm.getLat(),lon=sm.getLon(),acc=sm.getAccuracy();
          String message=getString(R.string.p_latitude)+": "+lat+getString(R.string.unit_deg)+"\n"+getString(R.string.p_longitude)+": "+lon+getString(R.string.unit_deg)+"\n"+getString(R.string.accuracy)+": ±"+acc+getString(R.string.unit_m);
          new AlertDialog.Builder(c)
            .setTitle(getString(R.string.gps_settings))
            .setMessage(message)
            .setPositiveButton(getString(R.string.OK),new DialogInterface.OnClickListener(){
              @Override
              public void onClick(DialogInterface dialog,int which){
                SharedPreferences.Editor editor=PreferenceManager.getDefaultSharedPreferences(c).edit();
                editor.putString(getString(R.string.p_latitude),String.valueOf(lat));
                editor.putString(getString(R.string.p_longitude),String.valueOf(lon));
                editor.apply();
              }
            })
            .setNegativeButton(getString(R.string.cancel),null)
            .show();
        }else{
          new AlertDialog.Builder(c)
            .setTitle(getString(R.string.error))
            .setMessage(getString(R.string.gps_settings_error))
            .setPositiveButton(getString(R.string.OK),null)
            .show();
        }
        return true;
      }
    });


    // 地図に表示
    findPreference(getString(R.string.p_showTarget)).setOnPreferenceClickListener(new Preference.OnPreferenceClickListener(){
      @Override
      public boolean onPreferenceClick(Preference preference){
      String uri="geo:0,0?q="+Parameters.destination[0]+","+Parameters.destination[1];
      Intent intent = new Intent();
      intent.setAction(Intent.ACTION_VIEW);
      intent.setClassName("com.google.android.apps.maps","com.google.android.maps.MapsActivity");
      intent.setData(Uri.parse(uri));
      startActivity(intent);
      return true;
      }
    });
    goal_judgement=(EditTextPreference)findPreference(getString(R.string.p_goalJudgement));
    goal_judgement.setOnPreferenceChangeListener(cnil);

    stuck_time=(EditTextPreference)findPreference(getString(R.string.p_stuckTime));
    stuck_time.setOnPreferenceChangeListener(cnil);
    stuck_num=(EditTextPreference)findPreference(getString(R.string.p_stuckNum));
    stuck_num.setOnPreferenceChangeListener(cnil);
    stuck_threshold=(EditTextPreference)findPreference(getString(R.string.p_stuckThreshold));
    stuck_threshold.setOnPreferenceChangeListener(cnil);

    threshold_light=(EditTextPreference)findPreference(getString(R.string.p_thresholdLightSensor));
    threshold_light.setOnPreferenceChangeListener(cnil);
    seconds_light=(EditTextPreference)findPreference(getString(R.string.p_secondsLightSensor));
    seconds_light.setOnPreferenceChangeListener(cnil);
    sleep_time=(EditTextPreference)findPreference(getString(R.string.p_sleepTime));
    sleep_time.setOnPreferenceChangeListener(cnil);
    threshold_accel=(EditTextPreference)findPreference(getString(R.string.p_thresholdAccel));
    threshold_accel.setOnPreferenceChangeListener(cnil);
    threshold_pressure=(EditTextPreference)findPreference(getString(R.string.p_thresholdPressure));
    threshold_pressure.setOnPreferenceChangeListener(cnil);
    seconds_landing=(EditTextPreference)findPreference(getString(R.string.p_secondsLanding));
    seconds_landing.setOnPreferenceChangeListener(cnil);
    separation_n=(EditTextPreference)findPreference(getString(R.string.p_separationN));
    separation_n.setOnPreferenceChangeListener(cnil);
    separation_time=(EditTextPreference)findPreference(getString(R.string.p_separationTime));
    separation_time.setOnPreferenceChangeListener(cnil);
    navigation_turn=(EditTextPreference)findPreference(getString(R.string.p_navigationTurn));
    navigation_turn.setOnPreferenceChangeListener(new MyPreferenceChangeListener(180));

    fold_degree=(EditTextPreference)findPreference(getString(R.string.p_foldDegree));
    fold_degree.setOnPreferenceChangeListener(new MyPreferenceChangeListener(180));
    run_degree=(EditTextPreference)findPreference(getString(R.string.p_runDegree));
    run_degree.setOnPreferenceChangeListener(new MyPreferenceChangeListener(180));
    turn_degree=(EditTextPreference)findPreference(getString(R.string.p_turnDegree));
    turn_degree.setOnPreferenceChangeListener(new MyPreferenceChangeListener(180));
    slow_speed=(EditTextPreference)findPreference(getString(R.string.p_slowSpeed));
    slow_speed.setOnPreferenceChangeListener(new MyPreferenceChangeListener(Parameters.motor_max_value));
    gps_timeout=(EditTextPreference)findPreference(getString(R.string.p_gpsTimeout));
    gps_timeout.setOnPreferenceChangeListener(cnil);
    gps_high_accuracy=(EditTextPreference)findPreference(getString(R.string.p_gpsHighAccuracy));
    gps_high_accuracy.setOnPreferenceChangeListener(cnil);
    rotation_degree=(EditTextPreference)findPreference(getString(R.string.p_rotationDegree));
    rotation_degree.setOnPreferenceChangeListener(new MyPreferenceChangeListener(359));
    circuit_port=(EditTextPreference)findPreference(getString(R.string.p_circuitPort));
    circuit_port.setOnPreferenceChangeListener(cnil);
    monitor_port=(EditTextPreference)findPreference(getString(R.string.p_monitorPort));
    monitor_port.setOnPreferenceChangeListener(cnil);

    calibration_song=(ListPreference)findPreference(getString(R.string.p_calibrationSong));
    try{
      String tmp=calibration_song.getValue();
      for(int i=0;i<list.length;i++){
        if(tmp.equals(list[i])){
          Parameters.calibration_song=i;
          break;
        }
        if(i==list.length-1)
          throw new Exception();
      }
    }catch(Exception e){
      calibration_song.setValueIndex(0);
      Parameters.calibration_song=0;
    }
    calibration_song.setSummary(list[Parameters.calibration_song]);


    // デフォルトの設定に戻す
    findPreference(getString(R.string.p_default)).setOnPreferenceClickListener(new Preference.OnPreferenceClickListener(){
      @Override
      public boolean onPreferenceClick(Preference preference){
        new AlertDialog.Builder(c)
          .setTitle(getString(R.string.default_title))
          .setMessage(getString(R.string.default_text))
          .setPositiveButton(getString(R.string.OK), new DialogInterface.OnClickListener(){
            @Override
            public void onClick(DialogInterface dialog,int which){
              Parameters.setDefaultValue();
              PreferenceManager.getDefaultSharedPreferences(c).edit().clear().commit();
              PreferenceManager.setDefaultValues(c,R.xml.preferences,true);
              Toast.makeText(c,getString(R.string.default_message),Toast.LENGTH_SHORT).show();
              ((Activity)c).finish();
            }
          })
          .setNegativeButton(getString(R.string.cancel),null)
          .show();
        return true;
      }
    });

    findPreference(getString(R.string.p_testSelfIntroduction)).setOnPreferenceClickListener(new Preference.OnPreferenceClickListener(){
      @Override
      public boolean onPreferenceClick(Preference preference){
        ((Core)Core.getContext()).getCentralSystem().getSoundManager().say2(Parameters.explain_text);
        return true;
      }
    });

    findPreference(getString(R.string.p_testOpening)).setOnPreferenceClickListener(new Preference.OnPreferenceClickListener(){
      @Override
      public boolean onPreferenceClick(Preference preference){
        ((Core)Core.getContext()).getCentralSystem().getSoundManager().say2(Parameters.opening_text);
        return true;
      }
    });

    findPreference(getString(R.string.p_about)).setOnPreferenceClickListener(new Preference.OnPreferenceClickListener(){
      @Override
      public boolean onPreferenceClick(Preference preference){
        String message=getString(R.string.app_name)+"\n"+getString(R.string.version)+" "+Parameters.version+"\n"+getString(R.string.build)+" "+Parameters.build+"\n"+getString(R.string.copyright)+"\n\n"+getString(R.string.warning);
        new AlertDialog.Builder(c)
          .setTitle(getString(R.string.p_about))
          .setIcon(R.mipmap.icon)
          .setMessage(message)
          .setPositiveButton(getString(R.string.close),null)
          .show();
        return true;
      }
    });
    Parameters.loadSetting();
    getPreferenceScreen().getSharedPreferences().registerOnSharedPreferenceChangeListener(this);
    updateSummary();
  }


  // 設定が変更されると呼ばれる
  @Override
  public void onSharedPreferenceChanged(SharedPreferences spf,String key){
    Parameters.loadSetting();
    ((Core)Core.getContext()).getCentralSystem().getLogger().writeActivity(Core.getContext().getString(R.string.tag_preference),Core.getContext().getString(R.string.log_preferenceChanged)+key);
    updateSummary();
  }

  private void showError(){
    new AlertDialog.Builder(c)
      .setTitle(getString(R.string.error))
      .setMessage(getString(R.string.inputError))
      .setPositiveButton(getString(R.string.OK),null)
      .show();
  }

  private void showNoInputError(){
    new AlertDialog.Builder(c)
      .setTitle(getString(R.string.error))
      .setMessage(getString(R.string.noInputError))
      .setPositiveButton(getString(R.string.OK),null)
      .show();
  }


  // サマリーに設定値を表示する
  private void updateSummary(){
    latitude.setSummary(Parameters.destination[0]+" "+Core.getContext().getString(R.string.unit_deg));
    longitude.setSummary(Parameters.destination[1]+" "+Core.getContext().getString(R.string.unit_deg));
    goal_judgement.setSummary(Parameters.threshold_goal+Core.getContext().getString(R.string.ps_m));
    stuck_time.setSummary(Parameters.threshold_time_stuck+Core.getContext().getString(R.string.ps_s));
    stuck_num.setSummary(String.valueOf(Parameters.threshold_n_stuck));
    stuck_threshold.setSummary(Parameters.threshold_distance_stuck+Core.getContext().getString(R.string.ps_m));
    threshold_light.setSummary(Parameters.threshold_light_sensor+" "+Core.getContext().getString(R.string.unit_lux));
    seconds_light.setSummary(Parameters.seconds_light_sensor+" "+Core.getContext().getString(R.string.unit_sec));
    sleep_time.setSummary(Parameters.sleep_time+" "+Core.getContext().getString(R.string.unit_sec));
    threshold_accel.setSummary(Parameters.threshold_accel_sensor+" "+Core.getContext().getString(R.string.unit_mss));
    threshold_pressure.setSummary(Parameters.threshold_pressure_sensor+" "+Core.getContext().getString(R.string.unit_hpa));
    seconds_landing.setSummary(Parameters.seconds_landing_sensor_check+" "+Core.getContext().getString(R.string.unit_sec));
    separation_n.setSummary(String.valueOf(Parameters.separating_n));
    separation_time.setSummary(Parameters.separating_time+" "+Core.getContext().getString(R.string.unit_ms));;
    navigation_turn.setSummary(Parameters.threshold_turn+" "+Core.getContext().getString(R.string.unit_deg));
    fold_degree.setSummary(Parameters.tailservo_fold+" "+Core.getContext().getString(R.string.unit_deg));
    run_degree.setSummary(Parameters.tailservo_run+" "+Core.getContext().getString(R.string.unit_deg));
    turn_degree.setSummary(Parameters.tailservo_turn+" "+Core.getContext().getString(R.string.unit_deg));
    slow_speed.setSummary(String.valueOf(Parameters.motor_speed_slowly));
    gps_timeout.setSummary(Parameters.gps_timeout+" "+Core.getContext().getString(R.string.unit_ms));
    gps_high_accuracy.setSummary(Parameters.gps_accuracy_threshold+" "+Core.getContext().getString(R.string.unit_m));
    rotation_degree.setSummary(Parameters.rotation_angle+" "+Core.getContext().getString(R.string.unit_deg));
    circuit_port.setSummary(String.valueOf(Parameters.port_circuit));
    monitor_port.setSummary(String.valueOf(Parameters.port_controller));
    calibration_song.setSummary(list[Parameters.calibration_song]);
  }



  // 入力空白チェック
  private class CheckNoInputListener implements Preference.OnPreferenceChangeListener{
    @Override
    public boolean onPreferenceChange(Preference preference,Object o){
      String txt=(String)o;
      if(txt.equals("")){
        showNoInputError();
        return false;
      }
      return true;
    }
  }


  // 入力値チェック
  private class MyPreferenceChangeListener implements Preference.OnPreferenceChangeListener{
    private int max_value;

    public MyPreferenceChangeListener(int max_value){
      this.max_value=max_value;
    }

    @Override
    public boolean onPreferenceChange(Preference preference,Object o){
      String txt=(String)o;
      if(txt.equals("")){
        showNoInputError();
        return false;
      }else if(Integer.valueOf(txt)>max_value){
        showError();
        return false;
      }
      return true;
    }
  }


}
