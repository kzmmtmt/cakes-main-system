package cakes.main_system;

import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;

import java.util.ArrayDeque;
import java.util.Queue;
import java.util.Timer;
import java.util.TimerTask;


// センサー(GPS含む)を管理
public class SensorsManager implements SensorEventListener, LocationListener{

  /// GPSがONになっているか確認
  // センサーが搭載されているかを確認

  private Context v;
  private SensorManager sm;
  private LocationManager lm;
  private double accel_x,accel_y,accel_z,azimuth,pitch,roll,light,pressure,magnet,lat,lon,alt,speed,bearing,accuracy,circuit_voltage=0; // 各センサーの値
  private double[][] accel, orientation;
  private boolean get_gps=false,high_accuracy=false,stuck=false,last_judgement=false; // GPSを取得出来たか, それの精度は高いか, 今スタックしているか
  private long gps_time=0; // GPSの取得時刻
  private Logger logger;
  private Timer gps_timer;
  private SoundManager sound;
  private Queue<Double> lat5,lon5;
  private int stuck_count=0;


  public SensorsManager(Context v){
    this.v=v;
    sm=(SensorManager)v.getSystemService(v.SENSOR_SERVICE);
    gps_timer=new Timer(true);
    lat5=new ArrayDeque<Double>();
    lon5=new ArrayDeque<Double>();
  }

  public void setSensors(SoundManager sound){
    this.sound=sound;
    try{
      lm=(LocationManager)v.getSystemService(v.LOCATION_SERVICE);
      lm.requestLocationUpdates("gps",1000,0,this);
    }catch(SecurityException e){
      new AlertDialog.Builder(v).setTitle(v.getString(R.string.error)).setMessage(v.getString(R.string.gps_permission_error)).setPositiveButton(v.getString(R.string.OK),null).show();
      logger.writeActivity(v.getString(R.string.tag_permission),v.getString(R.string.log_gpsPermissionDenied));
    }catch(Exception e){}

    /// 例外処理
    // 加速度センサー
    try{
      sm.registerListener(this,sm.getDefaultSensor(Sensor.TYPE_ACCELEROMETER),250000);
      accel=new double[2][3];
    }catch(Exception e){}

    // 傾きセンサー
    try{
    sm.registerListener(this,sm.getDefaultSensor(Sensor.TYPE_ORIENTATION),250000);
      orientation=new double[2][3];
    }catch(Exception e){}

    // 照度センサー
    try{
      sm.registerListener(this,sm.getDefaultSensor(Sensor.TYPE_LIGHT),1000000);
    }catch(Exception e){}

    // 気圧センサー
    try{
      sm.registerListener(this,sm.getDefaultSensor(Sensor.TYPE_PRESSURE),1000000);
    }catch(Exception e){}

    // 磁気センサー
    try{
      sm.registerListener(this,sm.getDefaultSensor(Sensor.TYPE_MAGNETIC_FIELD),1000000);
    }catch(Exception e){}
  }

  public void setLogger(Logger logger){
    this.logger=logger;
  }

  // バッテリー残量
  public int getBatteryLevel() {
    Intent battery=v.registerReceiver(null,new IntentFilter(Intent.ACTION_BATTERY_CHANGED));
    final int level=battery.getIntExtra("level",0);
    final int scale=battery.getIntExtra("scale",100);
    return level*100/scale;
  }


  // SensorsManagerの終了
  public void finish(){
    // センサー類の停止
    try{
      lm.removeUpdates(this); // GPSの停止
      sm.unregisterListener(this); // センサーの停止
    }catch(SecurityException e){
    }catch(Exception e){}
  }

  // GPSの値が取得できた時に呼ばれる
  private void gpsStarted(){
    sound.say(v.getString(R.string.gps_got));
    logger.writeActivity(v.getString(R.string.tag_gps),v.getString(R.string.log_gpsReceived));
  }


  // ログのテキスト出力
  public String getLoggingText(){
    return accel_x+","+accel_y+","+accel_z+","+azimuth+","+pitch+","+roll+","+light+","+pressure+","+magnet+","+lat+","+lon+","+alt+","+speed+","+bearing+","+accuracy+","+getBatteryLevel()+","+circuit_voltage;
  }

  // 画面を更新するテキストの出力(センサーモニター時)
  public String getScreenText(){
    String str=String.format("<b><font size=\"7\" color=\"cyan\">ACCELEROMETER</font></b><br> <font size=\"5\"><b>x</b>:</font> <font color=\"green\" size=\"5\">%f</font> [m/s^2]<br> <font size=\"5\"><b>y</b>:</font> <font color=\"green\" size=\"5\">%f</font> [m/s^2]<br> <font size=\"5\"><b>z</b>:</font> <font color=\"green\" size=\"5\">%f</font> [m/s^2]<br><br>",accel_x,accel_y,accel_z);
    str+=String.format("<b><font size=\"7\" color=\"cyan\">ORIENTATION</font></b><br> <font size=\"5\"><b>azimuth</b>:</font> <font color=\"green\" size=\"5\">%f</font> [°]<br> <font size=\"5\"><b>pitch</b>:</font> <font color=\"green\" size=\"5\">%f</font> [°]<br> <font size=\"5\"><b>roll</b>:</font> <font color=\"green\" size=\"5\">%f</font> [°]<br><br>",azimuth,pitch,roll);
    str+=String.format("<b><font size=\"7\" color=\"cyan\">LIGHT</font></b>: <font color=\"green\" size=\"5\">%f</font> [lux]<br>",light);
    str+=String.format("<b><font size=\"7\" color=\"cyan\">PRESSURE</font></b>: <font color=\"green\" size=\"5\">%f</font> [hPa]<br>",pressure);
    str+=String.format("<b><font size=\"7\" color=\"cyan\">MAGNETIC FIELD</font></b>: <font color=\"green\" size=\"5\">%f</font> [μT]<br><br>",magnet);
    if(get_gps)
      str+=String.format("<b><font size=\"7\" color=\"cyan\">GPS</font></b><br> <font size=\"5\"><b>latitude</b>:</font> <font color=\"green\" size=\"5\">%f</font> [°]<br> <font size=\"5\"><b>longitude</b>:</font> <font color=\"green\" size=\"5\">%f</font> [°]<br> <font size=\"5\"><b>altitude</b>:</font> <font color=\"green\" size=\"5\">%f</font> [m]<br> <font size=\"5\"><b>accuracy</b>:</font> <font color=\"green\" size=\"5\">%f</font> [m]<br> <font size=\"5\"><b>speed</b>:</font> <font color=\"green\" size=\"5\">%f</font> [m/s]<br> <font size=\"5\"><b>bearing</b>:</font> <font color=\"green\" size=\"5\">%f</font> [°]<br><br>",lat,lon,alt,accuracy,speed,bearing);
    else
      str+="<b><font size=\"7\" color=\"cyan\">GPS</font></b>: <font size=\"5\" color=\"yellow\">"+v.getString(R.string.loading_sensor)+"</font><br><br>";
    str+="<b><font size=\"7\" color=\"cyan\">BATTERY LIFE</font></b>: <font color=\"green\" size=\"5\">"+getBatteryLevel()+"</font> [%]<br>";
    str+=String.format("<b><font size=\"7\" color=\"cyan\">CIRCUIT VOLTAGE</font></b>: <font color=\"green\" size=\"5\">%.2f</font> [V]",circuit_voltage);
    return str;
  }

  // GPS
  @Override
  public void onLocationChanged(Location location){
    if(!get_gps){
      get_gps=true;
      gpsStarted();
    }
    lat=location.getLatitude(); // 緯度(deg)
    lon=location.getLongitude(); // 経度(deg)
    alt=location.getAltitude(); // 高度(m)
    accuracy=location.getAccuracy(); // 精度(m)
    high_accuracy=accuracy<=Parameters.gps_accuracy_threshold; // 精度が高ければtrueになる
    if(high_accuracy)
      stuckDetector(lat,lon);
    else // 精度が悪い時にはスタック判定しない
      stuck=false;
    speed=location.getSpeed(); // 速度(m/s)
    bearing=location.getBearing(); // 方位(deg)
    gps_time=location.getTime(); // 取得時刻(ms)
    startGpsTimer();
  }

  // スタックしているかの判定
  private void stuckDetector(double latitude,double longitude){
    lat5.add(latitude);
    lon5.add(longitude);
    if(lat5.size()>Parameters.threshold_time_stuck){
      float[] result=new float[3];
      Location.distanceBetween(latitude,longitude,lat5.remove(),lon5.remove(),result);
      boolean is_smaller_than_params=result[0]<=Parameters.threshold_distance_stuck;
      if(last_judgement!=is_smaller_than_params){
        stuck_count=0;
        last_judgement=is_smaller_than_params;
      }else{
        if(is_smaller_than_params)
          stuck_count++;
        else
          stuck_count--;
        if(Math.abs(stuck_count)>=Parameters.threshold_n_stuck)
          stuck=is_smaller_than_params;
      }
    }
  }

  // GPSが有効かどうかを検証する
  public void startGpsTimer(){
    try{
      gps_timer.cancel();
    }catch(Exception e){}
    gps_timer=new Timer(true);
    gps_timer.scheduleAtFixedRate(new TimerTask(){
      @Override
      public void run(){
        gps_timer.cancel();
        high_accuracy=false;
        get_gps=false;
        stuck=false; // GPSが取れていない時にはスタック判定しない
        sound.say(v.getString(R.string.gps_disconnect));
        logger.writeActivity(v.getString(R.string.tag_gps),v.getString(R.string.log_gpsDisconnected));
      }
    },Parameters.gps_timeout,Parameters.gps_timeout);
  }


  // GPS
  @Override
  public void onStatusChanged(String s,int i,Bundle bundle){}

  // GPS
  @Override
  public void onProviderEnabled(String s){}

  // GPS
  @Override
  public void onProviderDisabled(String s){}

  // Sensorの値が変わった時に呼ばれる
  @Override
  public void onSensorChanged(SensorEvent event){
    // 加速度・ジャイロ・傾きセンサーは3回分の平均値を用いる
    switch(event.sensor.getType()){
      case Sensor.TYPE_ACCELEROMETER: // 加速度センサー
        accel_x=(event.values[0]+accel[0][0]+accel[1][0])/3.0;
        accel_y=(event.values[1]+accel[0][1]+accel[1][1])/3.0;
        accel_z=(event.values[2]+accel[0][2]+accel[1][2])/3.0;
        accel[0]=accel[1];
        for(int i=0;i<3;i++)
          accel[1][i]=event.values[i];
        break;
      case Sensor.TYPE_ORIENTATION: // 傾きセンサー
        azimuth=(event.values[0]+orientation[0][0]+orientation[1][0])/3.0;
        pitch=(event.values[1]+orientation[0][1]+orientation[1][1])/3.0;
        roll=(event.values[2]+orientation[0][2]+orientation[1][2])/3.0;
        orientation[0]=orientation[1];
        for(int i=0;i<3;i++)
          orientation[1][i]=event.values[i];
        break;
      case Sensor.TYPE_LIGHT: // 照度センサー
        light=event.values[0];
        break;
      case Sensor.TYPE_PRESSURE: // 気圧センサー
        pressure=event.values[0];
        break;
      case Sensor.TYPE_MAGNETIC_FIELD: // 磁気センサー
        magnet=event.values[0];
        break;
    }
  }

  // Sensorの精度が変わった時に呼ばれる
  @Override
  public void onAccuracyChanged(Sensor sensor,int i){}

  public double getCircuitVoltage(){
    return circuit_voltage;
  }

  public void setCircuitVoltage(double circuit_voltage){
    this.circuit_voltage=circuit_voltage;
  }

  // 以下、getter
  public double getAccel_x(){
    return accel_x;
  }

  public double getAccel_y(){
    return accel_y;
  }

  public double getAccel_z(){
    return accel_z;
  }

  public double getAzimuth(){
    return azimuth;
  }

  public double getPitch(){
    return pitch;
  }

  public double getRoll(){
    return roll;
  }

  public double getLight(){
    return light;
  }

  public double getPressure(){
    return pressure;
  }

  public double getMagnet(){
    return magnet;
  }

  public double getLat(){
    return lat;
  }

  public double getLon(){
    return lon;
  }

  public double getAlt(){
    return alt;
  }

  public double getSpeed(){
    return speed;
  }

  public double getBearing(){
    return bearing;
  }

  public double getAccuracy(){
    return accuracy;
  }

  public long getGpsTime(){
    return gps_time;
  }

  public boolean isGetGps(){
    return get_gps;
  }

  public boolean isHighAccuracy(){
    return high_accuracy;
  }

  public boolean isStucking(){
    return stuck;
  }
}
