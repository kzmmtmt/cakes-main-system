package cakes.main_system;

import android.content.Context;

import java.util.Observable;

// シーケンスのベースとなるクラス
abstract public class Sequence extends Observable{
  protected String name; // シーケンス名
  protected Context c;
  protected Logger logger;
  protected SoundManager sm;
  protected CentralSystem cs;

  public Sequence(Context c){
    this.c=c;
    cs=((Core)Core.getContext()).getCentralSystem();
    logger=cs.getLogger();
    sm=cs.getSoundManager();
  }


  public String getName(){
    return name;
  }

  protected void onInit(String log_sequence,String draft){
    logger.writeActivity(c.getString(R.string.tag_sequence),log_sequence);
    logger.writeSequence(c.getString(R.string.tag_sequence),log_sequence);
    sm.say(draft+c.getString(R.string.sound_sequence_started));
  }

  protected void onInit(){} // 必ずオーバーライドして使う

  // 次のシーケンスに移行
  protected void onFinish(){
    setChanged();
    notifyObservers();
  }

  // 次のシーケンスに移行(ゴール時のみ)
  protected void onFinish(String message){
    setChanged();
    notifyObservers(message);
  }

  protected void cancelSequence(){} // 必ずオーバーライドして使う


}
