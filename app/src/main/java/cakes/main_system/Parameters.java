package cakes.main_system;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

// 各種パラメーター
public class Parameters{

  public static final int build=295;  // ビルド番号
  public static final String version="7.5.5"; // バージョン


  // ゴール地点のGPS座標
  public static double[] destination=new double[2];


  /* 通信関係 */
  public static int port_circuit; // 回路へのポート番号
  public static int port_controller; // コントローラーからのポート番号
  public static boolean show_at_mark; // @をログに表示する


  /* 機構 */
  public static final int motor_max_value=1000; // モーターの最大出力
  public static final int motor_min_value=0; // モーターの最小出力
  public static final int motor_turn_time=500; // 曲がるときの動作の時間[ms]
  public static int motor_speed_slowly; // 徐行速度[ms]
  public static double gps_accuracy_threshold; // GPSの精度がこれ以下だったら高精度だとする[m]
  public static int gps_timeout; // GPSの有効期限 //
  public static int rotation_angle; // スマホは右回りに何度傾いて設置されているか(0～359で設定)[deg]
  public static int tailservo_run; // 走行時の角度[deg]
  public static int tailservo_turn; // 転回時の角度[deg]
  public static int tailservo_fold; // 収納時の角度[deg]


  /* シーケンス関連 */
  public static boolean start_recording_in_allsequence; // オールシーケンス開始時に自動的に記録を開始する
  public static boolean opening; // オープニング
  public static String opening_text; // オープニングテキスト
  public static boolean calibration; // センサーのキャリブレーション行うかどうか
  public static int calibration_song; // 曲番号(1～)
  public static double threshold_light_sensor; // standbyで放出判定するのに必要な光の閾値[lux]
  public static int seconds_light_sensor; // standbyで放出判定するまでに必要な時間[s]
  public static double threshold_accel_sensor; // landingで加速度センサーのブレの許容値[m/s^2]
  public static int sleep_time; // 回路の電源を落とす時間[s]
  public static double threshold_pressure_sensor; // landingで気圧センサーのブレの許容値[hPa]
  public static int seconds_landing_sensor_check; // landingで着地判定するまでに必要な時間[s]
  public static final int separating_degree1=0; // separationのサーボの位置1[deg]
  public static final int separating_degree2=180; // separationのサーボの位置2[deg]
  public static int separating_time; // separating時のサーボを動かすのにかける時間[ms]
  public static int separating_n; // separating時にパラサーボを動かす数
  public static int threshold_turn; // navigation時に曲がる判断をする角度[deg]
  public static double threshold_goal; // navigation時のゴールと判断する接近距離[m]
  public static boolean escaping; // スタック判定をするかどうか
  public static int threshold_time_stuck; // この秒数以内で[s]
  public static int threshold_n_stuck; // この回数連続で
  public static double threshold_distance_stuck; // この距離以内の移動はスタックと見なす[m]

  public static String explain_text; // explainコマンド実行時に読み上げるテキスト


  public static void loadSetting(){
    Context c=Core.getContext();
    SharedPreferences p=PreferenceManager.getDefaultSharedPreferences(c);
    try{
      destination[0]=Double.valueOf(p.getString(c.getString(R.string.p_latitude),getDefaultValue(c.getString(R.string.p_latitude))));
      destination[1]=Double.valueOf(p.getString(c.getString(R.string.p_longitude),getDefaultValue(c.getString(R.string.p_longitude))));
      threshold_goal=Double.valueOf(p.getString(c.getString(R.string.p_goalJudgement),getDefaultValue(c.getString(R.string.p_goalJudgement))));
      escaping=p.getBoolean(c.getString(R.string.p_stuckOnOff),Boolean.valueOf(getDefaultValue(c.getString(R.string.p_stuckOnOff))));
      threshold_time_stuck=Integer.valueOf(p.getString(c.getString(R.string.p_stuckTime),getDefaultValue(c.getString(R.string.p_stuckTime))));
      threshold_n_stuck=Integer.valueOf(p.getString(c.getString(R.string.p_stuckNum),getDefaultValue(c.getString(R.string.p_stuckNum))));
      threshold_distance_stuck=Double.valueOf(p.getString(c.getString(R.string.p_stuckThreshold),getDefaultValue(c.getString(R.string.p_stuckThreshold))));
      start_recording_in_allsequence=p.getBoolean(c.getString(R.string.p_log),Boolean.valueOf(getDefaultValue(c.getString(R.string.p_log))));
      opening=p.getBoolean(c.getString(R.string.p_opening),Boolean.valueOf(getDefaultValue(c.getString(R.string.p_opening))));
      calibration=p.getBoolean(c.getString(R.string.p_calibration),Boolean.valueOf(getDefaultValue(c.getString(R.string.p_calibration))));
      threshold_light_sensor=Double.valueOf(p.getString(c.getString(R.string.p_thresholdLightSensor),getDefaultValue(c.getString(R.string.p_thresholdLightSensor))));
      seconds_light_sensor=Integer.valueOf(p.getString(c.getString(R.string.p_secondsLightSensor),getDefaultValue(c.getString(R.string.pd_secondsLightSensor))));
      sleep_time=Integer.valueOf(p.getString(c.getString(R.string.p_sleepTime),getDefaultValue(c.getString(R.string.p_sleepTime))));
      threshold_accel_sensor=Double.valueOf(p.getString(c.getString(R.string.p_thresholdAccel),getDefaultValue(c.getString(R.string.p_thresholdAccel))));
      threshold_pressure_sensor=Double.valueOf(p.getString(c.getString(R.string.p_thresholdPressure),getDefaultValue(c.getString(R.string.p_thresholdPressure))));
      seconds_landing_sensor_check=Integer.valueOf(p.getString(c.getString(R.string.p_secondsLanding),getDefaultValue(c.getString(R.string.p_secondsLanding))));
      separating_time=Integer.valueOf(p.getString(c.getString(R.string.p_separationTime),getDefaultValue(c.getString(R.string.pd_separationTime))));
      separating_n=Integer.valueOf(p.getString(c.getString(R.string.p_separationN),getDefaultValue(c.getString(R.string.p_separationN))));
      threshold_turn=Integer.valueOf(p.getString(c.getString(R.string.p_navigationTurn),getDefaultValue(c.getString(R.string.p_navigationTurn))));
      tailservo_fold=Integer.valueOf(p.getString(c.getString(R.string.p_foldDegree),getDefaultValue(c.getString(R.string.p_foldDegree))));
      tailservo_run=Integer.valueOf(p.getString(c.getString(R.string.p_runDegree),getDefaultValue(c.getString(R.string.p_runDegree))));
      tailservo_turn=Integer.valueOf(p.getString(c.getString(R.string.p_turnDegree),getDefaultValue(c.getString(R.string.p_turnDegree))));
      motor_speed_slowly=Integer.valueOf(p.getString(c.getString(R.string.p_slowSpeed),getDefaultValue(c.getString(R.string.p_slowSpeed))));
      gps_timeout=Integer.valueOf(p.getString(c.getString(R.string.p_gpsTimeout),getDefaultValue(c.getString(R.string.p_gpsTimeout))));
      gps_accuracy_threshold=Double.valueOf(p.getString(c.getString(R.string.p_gpsHighAccuracy),getDefaultValue(c.getString(R.string.p_gpsHighAccuracy))));
      rotation_angle=Integer.valueOf(p.getString(c.getString(R.string.p_rotationDegree),getDefaultValue(c.getString(R.string.p_rotationDegree))));
      port_circuit=Integer.valueOf(p.getString(c.getString(R.string.p_circuitPort),getDefaultValue(c.getString(R.string.p_circuitPort))));
      port_controller=Integer.valueOf(p.getString(c.getString(R.string.p_monitorPort),getDefaultValue(c.getString(R.string.p_monitorPort))));
      show_at_mark=p.getBoolean(c.getString(R.string.p_showAtMark),Boolean.valueOf(getDefaultValue(c.getString(R.string.p_showAtMark))));
      String name[]=c.getResources().getStringArray(R.array.calibration_entries);
      String tmp=p.getString(c.getString(R.string.p_calibrationSong),name[0]);
      for(int i=0;i<name.length;i++)
        if(tmp.equals(name[i])){
          calibration_song=i;
          break;
        }
      explain_text=p.getString(c.getString(R.string.p_selfIntroduction),getDefaultValue(c.getString(R.string.p_selfIntroduction)));
      opening_text=p.getString(c.getString(R.string.p_openingText),getDefaultValue(c.getString(R.string.p_openingText)));
    }catch(Exception e){
      setDefaultValue();
    }
  }

  public static void setDefaultValue(){
    Context c=Core.getContext();
    destination[0]=Double.valueOf(c.getString(R.string.dv_latitude));
    destination[1]=Double.valueOf(c.getString(R.string.dv_longitude));
    threshold_goal=Double.valueOf(c.getString(R.string.dv_goalJudgement));
    escaping=Boolean.valueOf(c.getString(R.string.dv_stuckJudgement));
    threshold_time_stuck=Integer.valueOf(c.getString(R.string.dv_stuckTime));
    threshold_n_stuck=Integer.valueOf(c.getString(R.string.dv_stuckNum));
    threshold_distance_stuck=Double.valueOf(c.getString(R.string.dv_stuckThreshold));
    start_recording_in_allsequence=Boolean.valueOf(c.getString(R.string.dv_log));
    opening=Boolean.valueOf(c.getString(R.string.dv_opening));
    calibration=Boolean.valueOf(c.getString(R.string.dv_calibration));
    threshold_light_sensor=Double.valueOf(c.getString(R.string.dv_thresholdLightSensor));
    seconds_light_sensor=Integer.valueOf(c.getString(R.string.dv_secondsLightSensor));
    sleep_time=Integer.valueOf(c.getString(R.string.dv_sleepTime));
    threshold_accel_sensor=Double.valueOf(c.getString(R.string.dv_thresholdAccel));
    threshold_pressure_sensor=Double.valueOf(c.getString(R.string.dv_thresholdPressure));
    seconds_landing_sensor_check=Integer.valueOf(c.getString(R.string.dv_secondsLanding));
    separating_time=Integer.valueOf(c.getString(R.string.dv_separationTime));
    separating_n=Integer.valueOf(c.getString(R.string.dv_separationN));
    threshold_turn=Integer.valueOf(c.getString(R.string.dv_navigationTurn));
    tailservo_fold=Integer.valueOf(c.getString(R.string.dv_foldDegree));
    tailservo_run=Integer.valueOf(c.getString(R.string.dv_runDegree));
    tailservo_turn=Integer.valueOf(c.getString(R.string.dv_turnDegree));
    motor_speed_slowly=Integer.valueOf(c.getString(R.string.dv_slowSpeed));
    gps_timeout=Integer.valueOf(c.getString(R.string.dv_gpsTimeout));
    gps_accuracy_threshold=Double.valueOf(c.getString(R.string.dv_gpsHighAccuracy));
    rotation_angle=Integer.valueOf(c.getString(R.string.dv_rotationDegree));
    port_circuit=Integer.valueOf(c.getString(R.string.dv_circuitPort));
    port_controller=Integer.valueOf(c.getString(R.string.dv_monitorPort));
    show_at_mark=Boolean.valueOf(c.getString(R.string.dv_showAtMark));
    calibration_song=0;
    explain_text=getDefaultValue(c.getString(R.string.p_selfIntroduction));
    opening_text=getDefaultValue(c.getString(R.string.p_openingText));
  }

  // デフォルト値の取得
  public static String getDefaultValue(String key){
    Context c=Core.getContext();
    if(key.equals(c.getString(R.string.p_latitude)))
      return c.getString(R.string.dv_latitude);
    if(key.equals(c.getString(R.string.p_longitude)))
      return c.getString(R.string.dv_longitude);
    if(key.equals(c.getString(R.string.p_goalJudgement)))
      return c.getString(R.string.dv_goalJudgement);
    if(key.equals(c.getString(R.string.pt_stuckJudgement)))
      return c.getString(R.string.dv_stuckJudgement);
    if(key.equals(c.getString(R.string.p_stuckTime)))
      return c.getString(R.string.dv_stuckTime);
    if(key.equals(c.getString(R.string.p_stuckNum)))
      return c.getString(R.string.dv_stuckNum);
    if(key.equals(c.getString(R.string.p_stuckThreshold)))
      return c.getString(R.string.dv_stuckThreshold);
    if(key.equals(c.getString(R.string.p_log)))
      return c.getString(R.string.dv_log);
    if(key.equals(c.getString(R.string.p_calibration)))
      return c.getString(R.string.dv_calibration);
    if(key.equals(c.getString(R.string.p_thresholdLightSensor)))
      return c.getString(R.string.dv_thresholdLightSensor);
    if(key.equals(c.getString(R.string.p_secondsLanding)))
      return c.getString(R.string.dv_secondsLanding);
    if(key.equals(c.getString(R.string.p_sleepTime)))
      return c.getString(R.string.dv_sleepTime);
    if(key.equals(c.getString(R.string.p_thresholdAccel)))
      return c.getString(R.string.dv_thresholdAccel);
    if(key.equals(c.getString(R.string.p_thresholdPressure)))
      return c.getString(R.string.dv_thresholdPressure);
    if(key.equals(c.getString(R.string.p_secondsLanding)))
      return c.getString(R.string.dv_secondsLanding);
    if(key.equals(c.getString(R.string.p_separationN)))
      return c.getString(R.string.dv_separationN);
    if(key.equals(c.getString(R.string.p_separationTime)))
      return c.getString(R.string.dv_separationTime);
    if(key.equals(c.getString(R.string.p_navigationTurn)))
      return c.getString(R.string.dv_navigationTurn);
    if(key.equals(c.getString(R.string.p_foldDegree)))
      return c.getString(R.string.dv_foldDegree);
    if(key.equals(c.getString(R.string.p_runDegree)))
      return c.getString(R.string.dv_runDegree);
    if(key.equals(c.getString(R.string.p_turnDegree)))
      return c.getString(R.string.dv_turnDegree);
    if(key.equals(c.getString(R.string.p_slowSpeed)))
      return c.getString(R.string.dv_slowSpeed);
    if(key.equals(c.getString(R.string.p_gpsTimeout)))
      return c.getString(R.string.dv_gpsTimeout);
    if(key.equals(c.getString(R.string.p_gpsHighAccuracy)))
      return c.getString(R.string.dv_gpsHighAccuracy);
    if(key.equals(c.getString(R.string.p_rotationDegree)))
      return c.getString(R.string.dv_rotationDegree);
    if(key.equals(c.getString(R.string.p_circuitPort)))
      return c.getString(R.string.dv_circuitPort);
    if(key.equals(c.getString(R.string.p_monitorPort)))
      return c.getString(R.string.dv_monitorPort);
    if(key.equals(c.getString(R.string.p_showAtMark)))
      return c.getString(R.string.dv_showAtMark);
    if(key.equals(c.getString(R.string.p_calibrationSong)))
      return "0";
    if(key.equals(c.getString(R.string.p_selfIntroduction)))
      return c.getString(R.string.dv_selfIntroduction);
    if(key.equals(c.getString(R.string.p_openingText)))
      return c.getString(R.string.dv_openingText);
    return "0";
  }
}
