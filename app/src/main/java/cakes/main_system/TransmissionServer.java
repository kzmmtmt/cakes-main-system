package cakes.main_system;


import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.List;
import java.util.Queue;
import java.util.concurrent.CopyOnWriteArrayList;

/**
 * Created by Takato on 2016/07/28.
 */
public class TransmissionServer extends Thread {
    ServerSocket serverSocket = null;
    Socket socket = null;
    BufferedReader in = null;
    BufferedWriter out = null;

    private SensorsManager sm;

    private int port;

    private boolean waiting;
    private boolean c = true;

    private String message;
    ArrayList<ServerSnedTime> sendTimeArrayList = new ArrayList<ServerSnedTime>();

    private Queue<String> messageQueue;

    private List<TcpObserver> observers = new CopyOnWriteArrayList<TcpObserver>();

    public TransmissionServer(int portNumber) {
        port = portNumber;
        waiting = true;
        messageQueue = new ArrayDeque<String>();
    }

    public void setSensorManager(SensorsManager sm){
        this.sm=sm;
    }

    public void send(String tag, String message) {
        boolean tmpMatch = false;

        for(int i = 0; i < sendTimeArrayList.size(); i++) {
            if(tag == sendTimeArrayList.get(i).tag) {
                if(!sendTimeArrayList.get(i).toGivePermissionSend()) {
                    return;
                }
                tmpMatch = true;
                break;
            }
        }

        if(!tmpMatch) {
            sendTimeArrayList.add(new ServerSnedTime(tag));
        }
        messageQueue.add(message);
    }

    @Override
    public void run() {
        long sendTime = System.currentTimeMillis();
        int sensorIndex = 0;
        while (c) {
            try {
                serverSocket = new ServerSocket(port);
                socket = serverSocket.accept();

                out = new BufferedWriter(new OutputStreamWriter(socket.getOutputStream()));
                in = new BufferedReader(new InputStreamReader(socket.getInputStream()));

                while (waiting) {
                    if(in.ready()) {
                        String str = in.readLine();
                        //if(!str.equals("ping"))
                        notifyObservers(str);
                    }

                    long now = System.currentTimeMillis();
                    if (messageQueue.peek() != null) {
                        out.write(messageQueue.poll());
                        out.newLine();
                        out.flush();
                        sendTime = now;
                    } else if (now - sendTime > 500 && port == 5555) {
                        if(sensorIndex == 0) {
                            out.write("latitude " + String.valueOf(sm.getLat()));
                            out.newLine();
                            out.flush();
                            sendTime = now;
                        } else if(sensorIndex == 1) {
                            out.write("longitude " + String.valueOf(sm.getLon()));
                            out.newLine();
                            out.flush();
                            sendTime = now;
                        } else if (sensorIndex == 2) {
                            out.write("roverBattery " + String.valueOf(sm.getBatteryLevel()));
                            out.newLine();
                            out.write("circuit " + String.valueOf(sm.getCircuitVoltage()));
                            out.newLine();
                            out.flush();
                            sensorIndex = -1;
                            sendTime = now;
                        }
                        sensorIndex++;
                    } else if (now - sendTime > 1000) {
                        out.write("ping");
                        out.newLine();
                        out.flush();
                        sendTime = now;
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                try {
                    if (serverSocket != null) {
                        serverSocket.close();
                    }
                    if (socket != null) {
                        socket.close();
                    }
                    if (in != null) {
                        in.close();
                    }
                    if (out != null) {
                        out.close();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
    }

    private void notifyObservers(String message) {
        for(TcpObserver observer: observers)
            observer.updateMessage(message);
    }

    public void addObserver(TcpObserver observer) {
        observers.add(observer);
    }

    public void removeObserver(TcpObserver observer) {
        observers.remove(observer);
        if(observers.isEmpty()) {
            destroyTransmission();
        }
    }

    public void destroyTransmission() {
        waiting = false;
        c = false;
        try {
            if (serverSocket != null) {
                serverSocket.close();
            }
            if (socket != null) {
                socket.close();
            }
            if (in != null) {
                in.close();
            }
            if (out != null) {
                out.close();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}

class ServerSnedTime {
    long sentTimeMillis;
    String tag;

    public ServerSnedTime(String tag) {
        this.tag = tag;
        sentTimeMillis = System.currentTimeMillis();
    }

    public boolean toGivePermissionSend() {
        long now = System.currentTimeMillis();
        if(now - this.sentTimeMillis > 100) {
            this.sentTimeMillis = now;
            return true;
        } else {
            return false;
        }
    }
}