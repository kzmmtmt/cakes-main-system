package cakes.main_system;

import android.app.Activity;
import android.app.Fragment;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

// 設定用アクティビティ
public class SettingActivity extends AppCompatActivity{

  private Logger logger;
  private SettingFragment sf;

  @Override
  protected void onCreate(Bundle savedInstanceState){
    super.onCreate(savedInstanceState);
    getSupportActionBar().show();
    getSupportActionBar().setDisplayShowHomeEnabled(true);
    getSupportActionBar().setIcon(android.R.drawable.ic_menu_preferences);
    setTitle(getString(R.string.preference));
    logger=((Core)Core.getContext()).getCentralSystem().getLogger();
    logger.writeActivity(getString(R.string.tag_activity),getString(R.string.log_startSetting));
    sf=new SettingFragment();
    getFragmentManager().beginTransaction().replace(android.R.id.content,sf).commit();
    sf.onAttach(this);
  }

  @Override
  public boolean onCreateOptionsMenu(Menu menu){
    getMenuInflater().inflate(R.menu.settings,menu);
    return true;
  }

  @Override
  protected void onResume(){
    super.onResume();
    sf.onAttach(this);
  }

  @Override
  public boolean onOptionsItemSelected(MenuItem item){
    // 閉じるボタン
    if(item.getItemId()==R.id.menu_close_setting){
      logger.writeActivity(getString(R.string.tag_activity),getString(R.string.log_finishSetting));
      onFinish();
      return true;
    }else if(item.getItemId()==R.id.menu_sensor_settings){
      Intent intent=new Intent(this,cakes.main_system.SensorMonitorActivity.class);
      startActivity(intent);
      return true;
    }
    return super.onOptionsItemSelected(item);
  }

  @Override
  // 戻るキーを無効化
  public boolean dispatchKeyEvent(KeyEvent event){
    if (event.getAction()==KeyEvent.ACTION_DOWN){
      switch(event.getKeyCode()){
        case KeyEvent.KEYCODE_BACK:
          Toast.makeText(this,getString(R.string.dont_back),Toast.LENGTH_SHORT).show();
          return true;
      }
    }
    return super.dispatchKeyEvent(event);
  }


  private void onFinish(){
    sf.onDestroyView();
    sf.onDestroy();
    sf.onDetach();
    finish();
  }

}
