package cakes.main_system;

import android.content.Context;

import java.util.Observable;
import java.util.Timer;
import java.util.TimerTask;

// 通信を管理
public class WirelessConnectionManager extends Observable{
  private TransmissionServer controllerTransmission = null;
  private TransmissionServer circuitTransmission = null;
  private Logger logger;
  private Context v;
  private Timer circuit_connection=null;
  private boolean circuit_connection_flag=false;

  public WirelessConnectionManager(Context v,Logger logger){
    this.v=v;
    this.logger=logger;
    connectToCircuit();
  }

  // 回路との通信を確立
  public void connectToCircuit(){
    if(circuitTransmission == null){
      circuitTransmission = new TransmissionServer(Parameters.port_circuit);
      circuitTransmission.addObserver(new TcpObserver(){
        @Override
        public void updateMessage(String message){
          try{
            circuit_connection.cancel();
          }catch(Exception e){}

          circuit_connection=new Timer(true);
          circuit_connection.scheduleAtFixedRate(new TimerTask(){
            @Override
            public void run(){
              cancel();
              circuit_connection_flag=false;
              logger.writeActivity(v.getString(R.string.tag_circuit),v.getString(R.string.log_circuitConnectionDisconnected));
              try{
                ((Core)Core.getContext()).getCentralSystem().getSoundManager().say(v.getString(R.string.sound_disconnected));
                disconnectToCircuit();
                connectToCircuit();
              }catch(Exception e){}
            }
          },5000,1000);

          if(!circuit_connection_flag){ // 通信が接続されたら
            circuit_connection_flag=true;
            logger.writeActivity(v.getString(R.string.tag_circuit),v.getString(R.string.log_circuitConnectionEstablished));
            try{
              ((Core)Core.getContext()).getCentralSystem().getSoundManager().say(v.getString(R.string.sound_connected));
            }catch(Exception e){}
          }
          try{
            ((Core)Core.getContext()).getCentralSystem().getSensorManager().setCircuitVoltage((float)(Integer.valueOf(message)*11)/1000);
            String s="battery="+(float)(Integer.valueOf(message)*11)/1000+"[V]";
            logger.writeCommunication(v.getString(R.string.tag_receive), v.getString(R.string.tag_circuit),s);
            setChanged();
            notifyObservers(s);
          }catch(Exception e){
            if((message.equals("@")&&Parameters.show_at_mark)||!message.equals("@")){ // @は無視
              logger.writeCommunication(v.getString(R.string.tag_receive),v.getString(R.string.tag_circuit),message);
              setChanged();
              notifyObservers(message);
            }
          }
          return;
        }
      });
      circuitTransmission.start();
      logger.writeActivity(v.getString(R.string.tag_connection), v.getString(R.string.log_connectToCircuit));
    }

  }

  // 回路との通信を切断
  public void disconnectToCircuit(){
    try{
      circuitTransmission.destroyTransmission();
      circuitTransmission = null;
      logger.writeActivity(v.getString(R.string.tag_connection),v.getString(R.string.log_disconnectToCircuit));
    }catch(Exception e){}
  }

  // 回路との通信ができているか
  public boolean isCircuitConnectionAlive(){
    return circuit_connection_flag;
  }

  // 回路にコマンドを送信
  public void sendToCircuit(String command){
    if(circuitTransmission!=null){
      circuitTransmission.send(String.valueOf(System.currentTimeMillis()),command);
      logger.writeCommunication(v.getString(R.string.tag_send),v.getString(R.string.tag_circuit),command);
    }
  }

  // コントローラーからの通信を確立
  public void connectToController(){
    if(controllerTransmission == null) {
      controllerTransmission = new TransmissionServer(Parameters.port_controller);
      controllerTransmission.addObserver(new TcpObserver() {
        @Override
        public void updateMessage(String message) {
          logger.writeCommunication(v.getString(R.string.tag_receive), v.getString(R.string.tag_controller), message);
          controllerTransmission.setSensorManager(((Core)Core.getContext()).getCentralSystem().getSensorManager());
          setChanged();
          notifyObservers(message);
        }
      });
      controllerTransmission.start();
      logger.writeActivity(v.getString(R.string.tag_connection), v.getString(R.string.log_connectToController));
    }
  }

  // コントローラーからの通信を切断
  public void disconnectToController(){
    try{
      controllerTransmission.destroyTransmission();
      controllerTransmission = null;
      logger.writeActivity(v.getString(R.string.tag_connection),v.getString(R.string.log_disconnectToController));
    }catch(Exception e){}
  }

  // 切断
  public void finish(){
    disconnectToController();
    disconnectToCircuit();
  }

  // コントローラーからのコマンドを変換する
  public String convertCommand(String str){
    String[] command=str.split(" ");
    if(command.length==2){
      switch(command[0]){
        case "r_motor":
        case "l_motor":
          if(command[1].equals("s"))
            return command[0]+" s";
          else{
            try{
              int value=10*Integer.parseInt(command[1]);
              return command[0]+" "+value;
            }catch(Exception e){}
          }
        case "led_r":
        case "led_g":
        case "led_b":
          try{
            int value=10*Integer.parseInt(command[1]);
            return command[0]+" "+value;
          }catch(Exception e){}
        case "t_servo":
        case "p_servo":
          try{
            int value=Integer.parseInt(command[1]);
            return command[0]+" "+value;
          }catch(Exception e){}
        case "sleep":
          try{
            int value=Integer.parseInt(command[1]);
            return command[0]+" "+value;
          }catch(Exception e){}
      }
    }
    return "";
  }



}
