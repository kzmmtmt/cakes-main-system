package cakes.main_system;

import android.content.Context;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;
import java.util.Observable;
import java.util.Observer;

// オールシーケンス
public class AllSequence implements Observer{

  private Context c;
  private List<Sequence> sequences;
  private int now=0; // 現在のシーケンス
  private boolean in_main_activity;
  private Logger logger;
  private SoundManager sm;

  public AllSequence(Context c,Boolean in_main_activity){
    this.c=c;
    this.in_main_activity=in_main_activity;
    logger=((Core)(Core.getContext())).getCentralSystem().getLogger();
    sequences=new ArrayList<Sequence>();
    sequences.add(new Opening(c));
    sequences.add(new Calibration(c));
    sequences.add(new Standby(c));
    sequences.add(new Landing(c));
    sequences.add(new Separation(c));
    sequences.add(new Navigation(c));
    sequences.add(new Escaping(c));
    sequences.add(new Congratulations(c));
    logger.recordSequence(false);
    for(Sequence s:sequences)
      s.addObserver(this);
    sm=((Core)(Core.getContext())).getCentralSystem().getSoundManager();
  }

  public void startSequence(int i){
    now=i;
    sequences.get(now).onInit();
  }

  public void startAllSequence(){
    logger.writeSequence(c.getString(R.string.tag_sequence),c.getString(R.string.log_sequence_start)+Parameters.destination[0]+","+Parameters.destination[1]+")");
    if(Parameters.start_recording_in_allsequence)
      logger.startAllLogging(false);
    logger.writeActivity(c.getString(R.string.tag_sequence),c.getString(R.string.log_sequence_start)+Parameters.destination[0]+","+Parameters.destination[1]+")");
    startSequence(0);
  }

  // シーケンスが終わると呼ばれる
  @Override
  public void update(Observable observable,Object o){
    if(observable.equals(sequences.get(sequences.size()-2))){
      now--;
      ((Navigation)(sequences.get(sequences.size()-3))).onRestart();
    if(in_main_activity)
      ((MainActivity)c).changeTextView(sequences.get(now).getName());
  }else{
    if(o==null){
      now++;
      startSequence(now);
      if(in_main_activity)
        ((MainActivity)c).changeTextView(sequences.get(now).getName());
    }else{
      if(o.toString().equals("goal")){
        now=7;
        logger.writeSequence(c.getString(R.string.tag_sequence),c.getString(R.string.log_sequence_end));
        logger.writeActivity(c.getString(R.string.tag_sequence),c.getString(R.string.log_sequence_end));
        startSequence(now);
        if(in_main_activity)
          ((MainActivity)c).setReset();
      }
    }
  }
}

  // オールシーケンスの終了
  public void onFinish(){
    for(Sequence s:sequences){
      try{
        s.cancelSequence();
      }catch(Exception e){}
    }
    logger.writeKml();
    logger.stopAllLogging(false);
    logger.stopSequenceLog(true);
  }
}
