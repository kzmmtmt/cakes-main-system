package cakes.main_system;

import android.app.Application;
import android.content.Context;

// グローバルな変数を格納
public class Core extends Application{
  private CentralSystem central_system;
  private static Context context;

  // 初期化
  public void Initialize(Context v,Context application_context){
    context=application_context;
    central_system=new CentralSystem(v);
  }

  public static Context getContext(){
    return context;
  }


  public CentralSystem getCentralSystem(){
    return central_system;
  }
}
